import logging
import sys
from utils.loaders import load_data
from utils.connectors import csv_connector, connector_utils as cu
from utils.mappers import yaml_mapper

log = logging.getLogger()


def create_metadata_mapper(fw, data, column_mappings, yaml_map, destination_object, namespace=""):

    if not column_mappings['project']:
        parent_project = fw.get_project(destination_object.parents.project)
        log.debug(f'setting project column to {parent_project.label}')
        data['project'] = parent_project.label
        column_mappings['project'] = 'project'

    if not column_mappings['group']:
        group = destination_object.parents.group
        log.debug(f'setting group column to {group}')
        data['group'] = group
        column_mappings['group'] = 'group'

    # if not column_mappings['subject']:
    #     parent_subject = fw.get_subject(destination_object.parents.subject)
    #     group = parent_project.group
    #     data['group'] = group
    #     column_mappings['group'] = 'group'

    if yaml_map:
        raw_map = yaml_mapper.YamlLoader(yaml_map)
        raw_map.load_map()
        metamapper = raw_map.generate_metamapper(fw)

    else:
        raw_map = yaml_mapper.YamlLoader()
        metamapper = raw_map.generate_default_mapper(fw, headers=set(data.keys()), container_mappings=column_mappings, namespace=namespace)



    log.debug(data)
    return metamapper


def create_connector(metamapper, data, dry_run, overwrite):

    connector = csv_connector.CSVConnector(metamapper, data, dryrun=dry_run, overwrite=overwrite)

    return connector


def main(
    fw,
    metadata_file,
    yaml_map=None,
    group_col=None,
    project_col=None,
    subject_col=None,
    session_col=None,
    acquisition_col=None,
    analysis_col=None,
    file_col=None,
    first_row=1,
    delimiter=",",
    overwrite=False,
    dry_run=False,
    destination_object=None,
    rows_are_groups=False,
    name_for_row_objects=""
):


    column_mappings = {
        "group": group_col,
        "project": project_col,
        "subject": subject_col,
        "session": session_col,
        "acquisition": acquisition_col,
        "analysis": analysis_col,
        "file": file_col,
    }

    # First attempt to load the data using the data loader.  If it's a supported filetype, this will
    # Return a dataframe.
    df = load_data.load_dataframe(metadata_file, first_row, delimiter=delimiter, sheet_spec=1)

    # Make the filename safe.  You'd be surprised at what people name things.
    namespace = cu.make_string_safe(metadata_file.stem)
    metamap = create_metadata_mapper(fw, df, column_mappings, yaml_map, destination_object, namespace)

    connector = create_connector(metamap, df, dry_run, overwrite)
    if not connector.validate_headers():
        raise Exception('invalid headers')

    if rows_are_groups:
        namespace = cu.make_string_safe(metadata_file.stem)

        if not name_for_row_objects:
            name_for_row_objects = namespace

        connector.rows_are_groups(name_for_row_objects)
        metamap = create_metadata_mapper(connector.fw, connector.data, column_mappings, yaml_map, destination_object)
        connector.metamap = metamap

    connector.process_data()


