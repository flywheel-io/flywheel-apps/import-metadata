```mermaid
graph TD;
    A[Metadata File]:::input --> J;
    B[Yaml Mapper]:::input -->|Overrules| D;
    D[Gear Config]:::input -->H;
    H[Extract:<br>- group<br>- project<br>- subject<br>- session<br>- acquisition<br>- analysis<br>- file] -->I;
    I{MetadataMapper}:::container-->K
    O{ObjectFinder}:::container-->I
    J{{determine file type}}-->K;
    K{DataConnector}:::container-->L
    ;
    L[process data]

    
    classDef container fill:#57d,color:#fff
    classDef input fill:#7a9,color:#fff
    classDef gear fill:#659,color:#fff
    linkStyle default color:red

```

``` mermaid
graph TD;
    A[Full Data] --> B

    subgraph Connector;
        subgraph update_containers
            B[single row]-->C
            C{MetadataMapper}:::container--locates-->D;
            D([container]):::input-->E
            B-->E
            E{{write_to_container}}:::gear
        end

    end;

    
    classDef container fill:#57d,color:#fff
    classDef input fill:#7a9,color:#fff
    classDef gear fill:#659,color:#fff
    linkStyle default color:red
```


``` mermaid
graph TD;
    B[Single Row Of Data] --> A
    B --> C
    B --> F
    subgraph MetadataMapper;
        A{{Container-Finder Map}}:::gear --Finds all container <br>information from data row-->C;
        C{ObjectFinder}:::container-->D;
        C--level--> E;
        D[Flywheel Container]
        E{{Level-Specific Map}}:::gear
    end;
    D-->F;
    E-->F;

    F[Output To Connector]
    
    classDef container fill:#57d,color:#fff
    classDef input fill:#7a9,color:#fff
    classDef gear fill:#659,color:#fff
    linkStyle default color:red
```

```mermaid
graph TD;

    A[Group]
    B[Project]
    C[Subject]
    D[Session]
    E[Acquisition]
    F[Analyis]
    G[File]
    H[Level]

    A --> I; 
    B --> I;
    C --> I;
    D --> I;
    E --> I;
    F --> I;
    G --> I;
    H --> I;
    subgraph id1[ ]
    I{{"find()"}}:::gear --> J

    J[Get highest level] --> LL;
    subgraph id2[iter loop]
    LL[current level]:::input --> LLL
    LLL{Parent Has<br>Finder?}
    LLL--No--> L
    LLL--Yes-->P;
    P{{"run parent query:<br>parent.TYPE.find()"}} --> k
    L{{"run client query:<br>fw.TYPE.find()"}} --> k
    k([container]):::container --> M;
    M{Lowest level?}
    M--No-->O;
    O[Use current container <br> as parent and search <br> for next lowest level]
    O--parent-->LL
    

    end
    M--Yes-->Q;
    Q{'Analysis' Level<br>Provided?}
    Q--No-->R
    Q--Yes--> S{{Get Analysis}}
    S-->T{{'File' Level<br>Provided?}}
    T--No-->R
    T--Yes-->U{{Get File}}
    U-->R
    R([return object]):::container

    end

    classDef container fill:#57d,color:#fff
    classDef input fill:#7a9,color:#fff
    classDef gear fill:#659,color:#fff
    linkStyle default color:red

```