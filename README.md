
# Import Metadata (import-metadata)

## Overview
### Summary
A gear for importing metadata into flywheel objects

### Cite
*license:* MIT
*url:* https://github.com/flywheel-apps/EP_MetadataImport
*cite:* NA

### Classification
*Category:* Analysis
*Gear Level:*

- [x] Project
- [x] Subject
- [x] Session
- [x] Acquisition
- [x] Analysis

----

[[_TOC_]]

----


### Inputs

#### metadata file
- **Name**: metadata_file
- **Type**: File
- **Optional**: False
- **Classification**: tabular/json
- **Description**: The file containing container mappings and metadata for upload
- **Notes**: The metadata file must have the correct format to be recognized by the gear.  
See the [metadata file spec](#metadata-file-specification) for more info


#### yaml map
- **Name**: yaml_map
- **Type**: File
- **Optional**: True
- **Classification**: yaml
- **Description**: a yaml file to implement more advanced metadata mappings
- **Notes**: The yaml map must be of valid map format.
See the [yaml map spec](#yaml-map-specification) for more info
  
  
### Config

#### group column
- **Name**: group_column
- **Type**: String
- **Description**: Name of the column header that has the group ID (default looks for column header 'group')
- **Default**: None

#### project column
- **Name**: project_column
- **Type**: String
- **Description**: Name of the column header that has the project ID/label (default looks for column header 'project')
- **Default**: None

#### subject column
- **Name**: subject_column
- **Type**: String
- **Description**: Name of the column header that has the subject ID/label (default looks for column header 'subject')
- **Default**: None

#### session column
- **Name**: session_column
- **Type**: String
- **Description**: Name of the column header that has the session ID/label (default looks for column header 'session')
- **Default**: None

#### acquisition column
- **Name**: acquisition_column
- **Type**: String
- **Description**: Name of the column header that has the acquisition ID/label (default looks for column header 'acquisition')
- **Default**: None

#### analysis column
- **Name**: analysis_column
- **Type**: String
- **Description**: Name of the column header that has the analysis ID/label (default looks for column header 'analysis')
- **Default**: None

#### file column
- **Name**: file_column
- **Type**: Integer
- **Description**: The first row that contains data (This includes the column headers)
- **Default**: None

#### delimiter
- **Name**: delimiter
- **Type**: String
- **Description**: The delimiter used (if file is a character-delimited file, such as CSV or TSV)
- **Default**: ","

#### metadata destination
- **Name**: metadata_destination
- **Type**: String
- **Description**: The location of the metadata fields to be uploaded to under 'info'. Defaults to CSV file name. Sub-categories are specified with a period, e.x. 'Health.InitialAssessment'
- **Default**: None

#### overwrite
- **Name**: overwrite
- **Type**: Boolean
- **Description**: Overwrite existing metadata information
- **Default**: False

#### gear log level
- **Name**: gear_log_level
- **Type**: String
- **Description**: The level at which the gear will log.  INFO = standard verbosity, DEBUG = more verbosity.
- **Default**: INFO

#### dry run
- **Name**: dry_run
- **Type**: Boolean
- **Description**: Only log what changes would be made, do not update anything.
- **Default**: False

### Outputs

#### Files

None

#### Metadata
Any metadata specified in the metadata file will be added to the specified containers/file objects in flywheel

## Usage

### Description

This gear imports metadata from a ***metadata file*** file into flywheel. The file contains ***mapping keys*** and
***metadata keys***.  This file is parsed so that a
***target object*** can be identified and metadata can be uploaded to it.  The ***metadata file*** can specify multiple
***target objects***, for example, a CSV file can specify one ***target object*** per row, and each row can contain 
different metadata to upload.  In this example, the column headers would be the keys, and the value row-by-row would
be the values.

- `metadata file`: a file containing metadata to be imported in key/value pairs, as well as information on which flywheel container to upload the data to
as well as information on which flywheel container to upload the data to.  This file generally contains **mapping keys** and **metadata keys**
- `mapping keys`: data key/value pairs that specify what the **target object** is for a given piece of metadata to be uploaded to. (for example, in a CSV, the keys are column headers, and the values are populated in each row)
- `metadata keys`: key/value pairs that specify metadata to be uploaded.  (for example, in a CSV, the keys are column headers, and the values are populated in each row)  
- `target object`: a flywheel container or file that is the intended destination for a given piece of metadata to be uploaed to.

Currently supported file types:
- CSV

When specifying a target object with mapping keys, you can specify group, project, subject, session, acquisition, 
analysis, and file.  By default, group and project are assumed to be the same as the one that the gear is being launched
in, however you CAN specify other groups/projects if desired.

> **NOTE:** Being MORE specific with your mapping keys will result in faster/more reliable code.  For example, when 
uploading metadata to an acquisition, it is better to specify a subject, session, AND acquisition rather than just 
subject/acquisition, session/acquisition, or just acquisition.   

Please see usage sections for individual filetypes for more formatting information.

#### `metadata file` specification
##### CSV
For a CSV file, each row contains metadata to be uploaded to a specific flywheel file/container.
The column headers are either **metadata keys**, which specify the metadata key to store the value under, OR they specify **mapping keys**, 
which contain flywheel container ID's or labels to identify which container to upload the metadata from a given row.

By default, flywheel will look for the following column headers to be mapping keys:
- `group`
- `project`
- `subject`
- `session`
- `acquisition`
- `analysis`
-  `file`

However, you can specify different column headers in the gear's config settings.
See the [basic CSV example](#basic-csv-data-import-without-a-mapping-file) for more information

#### `yaml map` specification

By default, the gear will import all columns in a csv file as metadata, aside from the columns used to locate 
containers.  Additionally, for each row, only one container can be specified.  For example, if you specify *project*,
*subject*, and *session* in a row, then all the remaining columns will be uploaded to the specified session.

However, the yaml mapping file allows you to choose not only which columns are uploaded, but which container they go
to.  You can specify any parent container of the lowest-specified container in the row.  In the example above, you could
specify that some columns go to the session, some columns go to the subject, some columns go to the project, and some
don't get uploaded at all. 

An example yaml file is shown here, and then its parts are broken down for more detail:

```yaml
container_map:
  project: 'cohort'
  subject: 'patient_number'
  session: 'measurement'

subject:
  import_map:
    - alignment

  rename_map:
    trial phase: level

  namespace:
    info.morality:
      - alignment

session:
  import_map:
    - age
    - z-score

  rename_map:
    average score: ave score
    adjusted score: adj score

  namespace:
    age:
      - age
    info.scores:
      - ave score
      - adj score

```

##### Overview


The major sections are outlined below, and then will be examined in more detail:
```yaml

# Each yaml file will start with a container_map, which simply indicates which columns to use to locate
# containers in flywheel.  If ommitted, the gear will assume that you're using the default column headers,
# ('group', 'project', 'subject', etc)
container_map:
 # This section can map which column headers correspond to what kind of container id in flywheel.  It sets the Mapping Keys


# All following sections are "metadata maps" for specific container types in flywheel.  
# Only necessary to include one for each type of container that you wish to upload metadata to.
project:
  # This section sets which column headers will be imported into the project level.
  # This can also rename columns and send columns to certain namespaces

subject:
  # This section sets which column headers will be imported into the subject level.
  # This can also rename columns and send columns to certain namespaces

session:
  # This section sets which column headers will be imported into the session level.
  # This can also rename columns and send columns to certain namespaces

acquisition:
  # This section sets which column headers will be imported into the acquisition level.
  # This can also rename columns and send columns to certain namespaces

analysis:
  # This section sets which column headers will be imported into the analysis level.
  # This can also rename columns and send columns to certain namespaces

file:
  # This section sets which column headers will be imported into the file level.
  # This can also rename columns and send columns to certain namespaces


```

##### container_map
The container map section can contain any or all of the following keys:
- `group`
- `subject`
- `session`
- `acquisition`
- `analysis`
- `file`

Each key is paired with a value, which corresponds to the column header that identifies the container type.
For example:

```yaml
container_map:
  project: 'cohort'
  subject: 'patient_number'
  session: 'measurement'

```

Indicates that the project ID or label is specified under the column with the header "cohort"

The subject ID or label is specified under the column with the header "patient_number"

The session ID or label is specified under the column with the header "measurement"

For more information, see  the [yaml map use case example](#csv-data-import-with-a-mapping-file)

##### metadata maps
For different container types (project, subject ,etc), you can specify exactly what columns get imported, what namespace
they go into in the metadata, and you can set what name they're stored under (different from the column header).

The format for these commands has the following sections:

```yaml

<container_type>:
  import_map:
    # This specifies the columns you wish to import to this particular container type.  Tt contains a list of strings
    # That match column headers
    - age
    - z-score

  rename_map:
    # This contains column headers that you wish to rename for metadata storage.  This contains key/value pairs, where
    # The key is the original column header in the CSV file, and the value is the name you want to store the data under
    # in the containers metadata.  
    average score: ave score
    adjusted score: adj score
  
  namespace:
    # This specifies different namespaces to upload metadata to.  By default all metadata is saved under `info.<file_name>`.
    # However, you may want to store the metadata at the container level (age, for a session), or in a different
    # level in the custom information.  This section consists of key/list pairs.  The keys are the new metadata
    # namespace to upload metadata to, and the lists are of the metadata info that you want to put in that namespace.
    # *NOTE* that if you rename a column, use the NEW NAME that the metadata will be stored under (see below)
    age:
      - age
    info.scores:
      - ave score
      - adj score

```


### Pre-requisites

#### Gear Pre-processing
This gear does not rely on any previous gear runs

#### Metadata
This gear does not have metadata requirements, however, this gear CAN overwrite existing metadata fields if the "overwrite" option is checked.
see 'Usage' for more info.



### Workflow

![generic gear workflow](source/images/import_metadata.png "Generic Workflow")

or

```mermaid
graph LR;
    A[Metadata File]:::input -->|Required| C;
    B[Yaml Mapper]:::input -->|Optional| C;
    C[Upload] --> D[Parent Container <br> Project, Subject, etc];
    D:::container --> E((Metadata Gear));
    E:::gear -->|metadata| F[container]:::container;
    E --> |metadata| G[container]:::container;
    E -.->|metadata| H[. . . . . . . .]:::container;
    E -->|metadata| I[container]:::container;
    
    classDef container fill:#57d,color:#fff
    classDef input fill:#7a9,color:#fff
    classDef gear fill:#659,color:#fff

```

The generic workflow is as follows:
1. A file containing metadata and container destination mapping is uploaded to flywheel (Usually at the project level)
   - optional: a yaml map file is uploaded to assist in metadata mapping

2. The metadata file is passed into the import-metadata gear as input (and optionally the yaml map)
3. the gear processes the metadata file, identifies containers/files to attatch metadata to, and does so sequentially.
   - By default, the gear uploads all metadata under a metadata object with the same name as the metadata file.  
    
### Use Cases

#### config `overqrite` option is selected

If the `overwrite` option is selected in the config, the existing metadata will be overwritten.  
Note that only the lowest level gets overwritten.  For example, if a subject has the following metadata:


#### Basic CSV data import without a mapping file

**Inputs:**
 - csv formatted metadata file

Uploading a csv file called "Initial_Exam_Results.csv" with the following
info:

| subject       | Mood        | Percent Correct | Duration of stay (min) |
| ----------- | ----------- | --------------- | ---------------------- |
| sub01  | happy | 89 | 23 |
| sub02  | sad | 75 | 34 |


1. the gear would identify `subject` as the only **mapping key**, and would assume that all other columns are metadata to be uploade
2. the gear would look for subjects with the labels "sub01" and "sub02" in  and would populate:

    Mood: "happy"
    Percent Correct: 89
    Duration of stay (min): 23
   
to `sub01.info.Initial_Exam_Results`:

![sub01 custom info](source/images/csv_example_01.png "sub01 custom info")


#### CSV data import with a mapping file

**Inputs:**
 - csv formatted metadata file
 - yaml mapping file


For this example, the following CSV file is provided:

| cohort  | patient_number    | measurement    | age | average score | adjusted score | trial phase | z-score | alignment       | fname         | acq        |
|------|------------|----------------|-----|---------------|----------------|-------------|---------|-----------------|---------------|------------|
| Work | topup_test | working_topup  | 1   | 58            | 68             | initial     | 0.34    | chaotic neutral | nodif.nii.gz  | nodif      |
| Work | CMRR       | CMRR           | 2   | 55            | 78             | final       | 0.98    | lawful evil     | metacopy.json | CMRR_Fail3 |
| Work | patient_02 | unzipped_dicom | 3   | 70            | 89             | wizard      | 0.666   | true neutral    |               |            |

along with the following yaml file:

```yaml
container_map:
  project: 'cohort'
  subject: 'patient_number'
  session: 'measurement'
  acquisition: 'acq'
  file: 'fname'

subject:
  import_map:
    - alignment

  rename_map:
    trial phase: level

  namespace:
    info.morality:
      - alignment

session:
  import_map:
    - age
    - z-score

  rename_map:
    average score: ave score
    adjusted score: adj score

  namespace:
    age:
      - age
    info.scores:
      - ave score
      - adj score
```

Let's compare what would happen if this was run WITH and WITHOUT the yaml map, row by row.
We will assume that the gear config is set up properly so that the project/subject/session/acquisition/file headers
can be found.

---
* ***Row 1***

WITHOUT yaml:
1. the gear would locate the file
1. The gear would extract the following values for the container to upload data to:
```
project:        Work
subject:        topup_test
session:        working_topup
acquisition:    nodif
file:           nodif.nii.gz
```

If it can locate this file successfully, it will upload all the metadata values with the same keys as the column 
headers as shown here:

![metadata on file](source/images/yaml_example_alltofile.png "metadata on file")

WITH yaml:
1. The gear would locate the file
1. the gear would look for the LOWEST container level specified in the yaml map.  
    - NOTE: since there is no 'file' level, no metadata is uploaded to the file
1. The gear would find "Session" as the lowest container level specified in the yaml map, so it would move to the file's 
   parent session, and upload the following metadata:
    - ```
      ave score: 58
      adj score: 68
      ```
      to the session container's `info.scores` metadata   
    - `1`
       to the sessions `age` metadata
    - `z-score: 0.34`  to the session container's `info.<CSV FILE NAME>` metadata
1. The gear would then look for the NEXT lowest container specified by the yaml map, and would find "subject"
1. The gear would load the parent subject, and upload the following metadata to it:
    - `alignment: chaotic neutral` to the subject's `info.morality` metadata
    - `level: initial` to the subject's `info.<CSV FILE NAME>` metadata

    

### Logging

This gear will log information the help you follow the progress of the metadata ingest.

The log is roughly broken into 3 parts:  First, the configuration settings are logged:

```
[ 20210225 17:41:21     INFO flywheel_gear_toolkit.logging] Log level is INFO
[ 20210225 17:41:21     INFO flywheel_gear_toolkit.context.context] Destination is analysis=6037e06f8afc643bad6e8ace
[ 20210225 17:41:21     INFO flywheel_gear_toolkit.context.context] Input file "csv_file" is Data_Entry_2017_test.csv from project=6037d56c6e67757f166e8aa6
[ 20210225 17:41:21     INFO flywheel_gear_toolkit.context.context] Config "attached_files=True"
[ 20210225 17:41:21     INFO flywheel_gear_toolkit.context.context] Config "delimiter=,"
[ 20210225 17:41:21     INFO flywheel_gear_toolkit.context.context] Config "dry_run=False"
[ 20210225 17:41:21     INFO flywheel_gear_toolkit.context.context] Config "first_row=1"
[ 20210225 17:41:21     INFO flywheel_gear_toolkit.context.context] Config "gear_log_level=INFO"
[ 20210225 17:41:21     INFO flywheel_gear_toolkit.context.context] Config "mapping_column="
[ 20210225 17:41:21     INFO flywheel_gear_toolkit.context.context] Config "object_type=acquisition"
[ 20210225 17:41:21     INFO flywheel_gear_toolkit.context.context] Config "overwrite=True"
[ 20210225 17:41:21     INFO utils.load_data] No object column specified, assuming column 1
[ 20210225 17:41:21     INFO utils.load_data] Using column Image Index
[ 20210225 17:41:22     INFO __main__] Starting Mapping
```


Following this, each row will generate a report.  For example, given the row:

| Image Index | Finding Labels | Follow-up # | Patient ID | Patient Age | Patient Gender | View Position | "OriginalImage[Width,Height]" | "OriginalImagePixelSpacing[x,y]" |
| ----------- | -------------- | ----------- | ---------- | ----------- | -------------- | ------------- | ----------------------------- | -------------------------------- |
| 00000340_239.png | No Finding | 3 | 340 | 33 | F | AF | "3532,3451" | "0.033,0.643" |


The output may look like this:

```[ 20210225 17:41:22     INFO __main__] 
==================================================
Setting Metadata For 00000500_009.png
--------------------------------------------------
[ 20210225 17:41:22     INFO __main__] Image Index                       00000340_239.png
Finding Labels                          No Finding
Follow-up #                                      3
Patient ID                                     340
Patient Age                                     33
Patient Gender                                   F
View Position                                   AF
OriginalImage[Width,Height]              3532,3451
OriginalImagePixelSpacing[x,y]         0.033,0.643

Name: 0, dtype: object
[ 20210225 17:41:22     INFO __main__] updating {'Data_Entry_2017_test': {'csv': {'Finding Labels': 'No Finding', 'Follow-up #': 3, 'Patient ID': 340, 'Patient Age': 33, 'Patient Gender': 'F', 'View Position': 'AF', 'OriginalImage[Width,Height]': '3532,3451', 'OriginalImagePixelSpacing[x,y]': '0.033,0.643'}}
[ 20210225 17:41:22     INFO __main__] 
--------------------------------------------------
STATUS: Success
==================================================
```

Finally, at the end, a summary will be given on the whole process:
```
===============================================================================
Final Report: 10/11 objects updated successfully
90.9090909090909%
See output report file for more details
===============================================================================
```

## Contributing

[For more information about how to get started contributing to that gear,
checkout [CONTRIBUTING.md](CONTRIBUTING.md).]
