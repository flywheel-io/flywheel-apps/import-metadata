from utils.mappers.yaml_mapper import ContainerMap
from unittest.mock import MagicMock
import pytest
import pandas as pd

GROUP = "group"
PROJECT = "project"
SUBJECT = "subject"
SESSION = "session"
ACQUISITION = "acquisition"
ANALYSIS = "analysis"
FILE = "file"
FW = "fw"

CURRENT_DATA = {
    GROUP: f"{GROUP}_val",
    PROJECT: f"{PROJECT}_val",
    SUBJECT: f"{SUBJECT}_val",
    SESSION: f"{SESSION}_val",
    ACQUISITION: f"{ACQUISITION}_val",
    ANALYSIS: f"{ANALYSIS}_val",
    FILE: f"{FILE}_val",
}


def test_initializer():

    container_map = ContainerMap(
        GROUP, PROJECT, SUBJECT, SESSION, ACQUISITION, ANALYSIS, FILE, FW
    )

    assert container_map.group == GROUP
    assert container_map.project == PROJECT
    assert container_map.subject == SUBJECT
    assert container_map.session == SESSION
    assert container_map.acquisition == ACQUISITION
    assert container_map.analysis == ANALYSIS
    assert container_map.file == FILE
    assert container_map.fw == FW


def test_lowest_level():

    container_map = ContainerMap(group=GROUP, fw=FW)
    assert container_map.lowest_level() == GROUP

    container_map.project = PROJECT
    assert container_map.lowest_level() == PROJECT

    container_map.subject = SUBJECT
    assert container_map.lowest_level() == SUBJECT

    container_map.session = SESSION
    assert container_map.lowest_level() == SESSION

    container_map.acquisition = ACQUISITION
    assert container_map.lowest_level() == ACQUISITION

    container_map.analysis = ANALYSIS
    assert container_map.lowest_level() == ANALYSIS

    container_map.file = FILE
    assert container_map.lowest_level() == FILE


def test_get_values():

    container_map = ContainerMap(
        GROUP, PROJECT, SUBJECT, SESSION, ACQUISITION, ANALYSIS, FILE, FW
    )

    CURRENT_DATA = {
        GROUP: f"{GROUP}_val",
        PROJECT: f"{PROJECT}_val",
        SUBJECT: f"{SUBJECT}_val",
        ACQUISITION: f"{ACQUISITION}_val",
        ANALYSIS: f"{ANALYSIS}_val",
        FILE: f"{FILE}_val",
    }

    current_data = pd.Series(CURRENT_DATA)

    (
        ret_group,
        ret_project,
        ret_subject,
        ret_session,
        ret_acquisition,
        ret_analysis,
        ret_file,
    ) = container_map.get_values(current_data)

    assert ret_group == f"{GROUP}_val"
    assert ret_project == f"{PROJECT}_val"
    assert ret_subject == f"{SUBJECT}_val"
    assert ret_session is None
    assert ret_acquisition == f"{ACQUISITION}_val"
    assert ret_analysis == f"{ANALYSIS}_val"
    assert ret_file == f"{FILE}_val"


def test_get_container_multiple_containers():
    # Requires mocking of FlywheelObjectLinker.  Perhaps refactor?

    container_map = ContainerMap(
        GROUP, PROJECT, SUBJECT, SESSION, ACQUISITION, ANALYSIS, FILE, FW
    )

    CURRENT_DATA = {
        GROUP: f"{GROUP}_val",
        PROJECT: f"{PROJECT}_val",
        SUBJECT: f"{SUBJECT}_val",
        SESSION: f"{SESSION}_val",
        ACQUISITION: f"{ACQUISITION}_val",
        ANALYSIS: f"{ANALYSIS}_val",
        FILE: f"{FILE}_val",
    }
    current_data = pd.Series(CURRENT_DATA)
    container_map.link.find = MagicMock(return_value=[1, 2])
    with pytest.raises(Exception) as e_info:
        container_map.get_container(current_data)
    assert str(e_info.value) == "Multiple matches found for group='group_val'/project='project_val'/subject='subject_val'/session='session_val'/acquisition='acquisition_val'/analysis='analysis_val'/file='file_val'"


def test_get_container_zero_containers():
    # Requires mocking of FlywheelObjectLinker.  Perhaps refactor?

    container_map = ContainerMap(
        GROUP, PROJECT, SUBJECT, SESSION, ACQUISITION, ANALYSIS, FILE, FW
    )

    CURRENT_DATA = {
        GROUP: f"{GROUP}_val",
        PROJECT: f"{PROJECT}_val",
        SUBJECT: f"{SUBJECT}_val",
        SESSION: f"{SESSION}_val",
        ACQUISITION: f"{ACQUISITION}_val",
        ANALYSIS: f"{ANALYSIS}_val",
        FILE: f"{FILE}_val",
    }
    current_data = pd.Series(CURRENT_DATA)
    container_map.link.find = MagicMock(return_value=[])
    with pytest.raises(Exception) as e_info:
        container_map.get_container(current_data)
    assert str(e_info.value) == "Could not locate group='group_val'/project='project_val'/subject='subject_val'/session='session_val'/acquisition='acquisition_val'/analysis='analysis_val'/file='file_val'"


def test_get_container():
    # Requires mocking of FlywheelObjectLinker.  Perhaps refactor?

    container_map = ContainerMap(
        GROUP, PROJECT, SUBJECT, SESSION, ACQUISITION, ANALYSIS, FILE, FW
    )

    CURRENT_DATA = {
        GROUP: f"{GROUP}_val",
        PROJECT: f"{PROJECT}_val",
        SUBJECT: f"{SUBJECT}_val",
        SESSION: f"{SESSION}_val",
        ACQUISITION: f"{ACQUISITION}_val",
        ANALYSIS: f"{ANALYSIS}_val",
        FILE: f"{FILE}_val",
    }
    current_data = pd.Series(CURRENT_DATA)
    container_map.link.find = MagicMock(return_value=["container"])
    result = container_map.get_container(current_data)
    assert result == "container"
