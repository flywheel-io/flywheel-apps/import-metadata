from utils.mappers.yaml_mapper import ContainerLevelMap, ElementLevelMap
from utils.mappers.yaml_mapper import NAMESPACE, IMPORT, RENAME

import copy
import pytest
import logging

logging.basicConfig(level="DEBUG")
log = logging.getLogger()

GROUP = "group"
PROJECT = "project"
SUBJECT = "subject"
SESSION = "session"
ACQUISITION = "acquisition"
ANALYSIS = "analysis"
FILE = "file"
FW = "fw"

CURRENT_DATA = {
    GROUP: f"{GROUP}_val",
    PROJECT: f"{PROJECT}_val",
    SUBJECT: f"{SUBJECT}_val",
    SESSION: f"{SESSION}_val",
    ACQUISITION: f"{ACQUISITION}_val",
    ANALYSIS: f"{ANALYSIS}_val",
    FILE: f"{FILE}_val",
}

VALID_KEY = "age"
DATA_MAP = {
    IMPORT: ["import_header_1", "namespace_header_1"],
    RENAME: {"rename_orig": "rename_new"},
    NAMESPACE: {VALID_KEY: ["namespace_header_1"]},
}

DATA_MAP_UNSAFE = {
    IMPORT: ["new @#"],
    RENAME: {"rename_orig": "rename_new"},
    NAMESPACE: {VALID_KEY: ["namespace_header_1"]},
}

INVALID_KEY = "invalid_namespace"
DATA_MAP_INVALID = {
    IMPORT: ["import_header_1"],
    RENAME: {"rename_orig": "rename_new"},
    NAMESPACE: {INVALID_KEY: ["namespace_header_1"]},
}


def test_init():
    container_level_map = ContainerLevelMap(copy.deepcopy(DATA_MAP), "level")

    assert container_level_map.data_map == DATA_MAP
    assert container_level_map.level == "level"


def test_validate_namespaces_valid():
    container_level_map = ContainerLevelMap(None)
    container_level_map.data_map = copy.deepcopy(DATA_MAP)

    assert container_level_map.validate_namespaces()


def test_validate_namespaces_invalid():
    container_level_map = ContainerLevelMap(None)
    container_level_map.data_map = copy.deepcopy(DATA_MAP_INVALID)
    container_level_map.raw = copy.deepcopy(container_level_map.data_map)

    with pytest.raises(Exception) as e_info:
        container_level_map.validate_namespaces()
    assert f"namespace {INVALID_KEY} is not valid" in str(e_info.value)


def test_validate_keys_valid():
    container_level_map = ContainerLevelMap(None)
    container_level_map.data_map = copy.deepcopy(DATA_MAP)
    container_level_map.raw = copy.deepcopy(container_level_map.data_map)

    container_level_map.validate_namespaces()

    assert container_level_map.validate_keys()


def test_validate_keys_duplicate():
    container_level_map = ContainerLevelMap(None)
    container_level_map.data_map = copy.deepcopy(DATA_MAP)
    container_level_map.data_map[RENAME] = {"rename_orig": "import_header_1"}
    container_level_map.raw = copy.deepcopy(container_level_map.data_map)

    container_level_map.validate_namespaces()

    with pytest.raises(Exception) as e_info:
        container_level_map.validate_keys()
    assert "Duplicate Import name" in str(e_info.value)


def test_validate_keys_safe_name():
    container_level_map = ContainerLevelMap(None)
    container_level_map.data_map = copy.deepcopy(DATA_MAP_UNSAFE)
    # container_level_map.data_map[IMPORT] = ['new @#']
    container_level_map.raw = copy.deepcopy(container_level_map.data_map)

    container_level_map.validate_namespaces()
    container_level_map.validate_keys()
    log.debug(container_level_map.data_map)

    assert container_level_map.data_map[IMPORT] == []
    assert container_level_map.data_map[RENAME]["new @#"] == "new__at__num_"


def test_generate_maps_valid():

    container_level_map = ContainerLevelMap(None)
    container_level_map.data_map = copy.deepcopy(DATA_MAP)
    container_level_map.raw = copy.deepcopy(container_level_map.data_map)

    container_level_map.validate_namespaces()
    container_level_map.validate_keys()
    container_level_map.generate_maps()

    log.info(container_level_map.maps)
    log.info(container_level_map.data_map)
    print(container_level_map.maps)

    assert (
        ElementLevelMap(None, "import_header_1", "info.import_header_1")
        in container_level_map.maps
    )
    assert (
        ElementLevelMap(None, "rename_orig", "info.rename_new")
        in container_level_map.maps
    )
    assert (
        ElementLevelMap(None, "namespace_header_1", f"{VALID_KEY}")
        in container_level_map.maps
    )


def test_get_source_keys():

    container_level_map = ContainerLevelMap(copy.deepcopy(DATA_MAP), "level")
    keys = container_level_map.get_source_keys()

    assert len(keys) == 3
    assert "import_header_1" in keys
    assert "namespace_header_1" in keys
    assert "rename_orig" in keys


def test_get_update_items():

    container_level_map = ContainerLevelMap(copy.deepcopy(DATA_MAP), "level")
    container_level_map.get_update_items()

    assert len(container_level_map.update_items) == 2
    assert "info" in container_level_map.update_items
    assert VALID_KEY in container_level_map.update_items
