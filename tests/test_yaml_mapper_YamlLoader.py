import os
from utils.mappers.yaml_mapper import YamlLoader, ContainerMap, ContainerLevelMap
import pytest
import yaml
import flywheel

CONTAINER_MAP = {
    "project": "cohort",
    "subject": "patient_number",
    "session": "measurement",
    "acquisition": "sequence",
    "file": "scan",
}
PROJECT_MAP = {"import_map": ["project_meta"]}
SUBJECT_MAP = {"import_map": ["subject_meta"]}
SESSION_MAP = {"import_map": ["age", "session_meta"], "namespace": {"age": ["age"]}}
ACQUISITION_MAP = {"import_map": ["acq_meta"]}
FILE_MAP = {"import_map": ["file_meta"]}
yaml_dict = {
    "container_map": CONTAINER_MAP,
    "project": PROJECT_MAP,
    "subject": SUBJECT_MAP,
    "session": SESSION_MAP,
    "acquisition": ACQUISITION_MAP,
    "file": FILE_MAP,
}


YAML_FILE = "data.yaml"

with open(YAML_FILE, "w") as outfile:
    yaml.dump(yaml_dict, outfile, default_flow_style=False)

TXT_FILE = "data.txt"
with open(TXT_FILE, "w") as outfile:
    outfile.write("garbage")


def test_initializer():
    ym = YamlLoader({"data": "map"})
    assert ym.data_map == {"data": "map"}
    assert isinstance(ym.container_map, ContainerMap)
    assert ym.MetaMaps == []


def test_load_map_yamlfile():
    ym = YamlLoader(YAML_FILE)
    ym.load_map()

    assert ym.data_map == {
        "project": PROJECT_MAP,
        "subject": SUBJECT_MAP,
        "session": SESSION_MAP,
        "acquisition": ACQUISITION_MAP,
        "file": FILE_MAP,
    }


def test_load_map_txtfile():
    ym = YamlLoader(TXT_FILE)
    with pytest.raises(TypeError) as e_info:
        ym.load_map()
    assert (
        str(e_info.value)
        == "data_map must be of type dict or a yaml file ending in '.yaml'"
    )


def test_find_object_locator_columns():
    ym = YamlLoader(YAML_FILE)
    ym.load_map()
    assert ym.container_info == CONTAINER_MAP
    assert ym.container_map == ContainerMap(**CONTAINER_MAP)


def test_find_object_locator_columns_blank():
    ym = YamlLoader()

    ym.data_map = {
        "project": PROJECT_MAP,
        "subject": SUBJECT_MAP,
        "session": SESSION_MAP,
        "acquisition": ACQUISITION_MAP,
        "file": FILE_MAP,
    }

    ym.find_object_locator_columns()
    container_info = {
        "group": "group",
        "project": "project",
        "subject": "subject",
        "session": "session",
        "acquisition": "acquisition",
        "analysis": "analysis",
        "file": "file",
    }
    assert ym.container_info == container_info
    assert ym.container_map == ContainerMap(**container_info)


def test_generate_metamapper():
    fw = flywheel.Client(os.environ["FWGA_API"])
    ym = YamlLoader(YAML_FILE)
    ym.load_map()
    metamap = ym.generate_metamapper(fw)

    assert metamap.container_map == ym.container_map
    assert metamap.project_map == ContainerLevelMap(PROJECT_MAP, "project")
    assert metamap.subject_map == ContainerLevelMap(SUBJECT_MAP, "subject")
    assert metamap.session_map == ContainerLevelMap(SESSION_MAP, "session")
    assert metamap.acquisition_map == ContainerLevelMap(ACQUISITION_MAP, "acquisition")
    assert metamap.file_map == ContainerLevelMap(FILE_MAP, "file")

    # Negative assertions
    assert metamap.file_map != ContainerLevelMap(FILE_MAP, "wrong level")
    assert metamap.file_map != ContainerLevelMap(PROJECT_MAP, "file")


def test_generate_connector():
    # I don't think this is used at the moment.

    pass


def test_generate_default_mapper_empty():
    fw = flywheel.Client(os.environ["FWGA_API"])
    ym = YamlLoader()
    metamap = ym.generate_default_mapper(fw)

    default_map = {
        "group": "group",
        "project": "project",
        "subject": "subject",
        "session": "session",
        "acquisition": "acquisition",
        "analysis": "analysis",
        "file": "file",
    }
    assert metamap.container_map == ContainerMap(fw=fw, **default_map)


def test_cleanup():
    os.remove(YAML_FILE)
    os.remove(TXT_FILE)

    kirby_dance = "(>'-')> <('-'<) ^('-')^ v('-')v(>'-')> (^-^)"
    assert kirby_dance == "(>'-')> <('-'<) ^('-')^ v('-')v(>'-')> (^-^)"
