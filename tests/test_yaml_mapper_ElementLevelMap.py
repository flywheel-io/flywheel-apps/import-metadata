import logging

from utils.mappers.yaml_mapper import ElementLevelMap

GROUP = "group"
PROJECT = "project"
SUBJECT = "subject"
SESSION = "session"
ACQUISITION = "acquisition"
ANALYSIS = "analysis"
FILE = "file"
FW = "fw"

CURRENT_DATA = {
    GROUP: f"{GROUP}_val",
    PROJECT: f"{PROJECT}_val",
    SUBJECT: f"{SUBJECT}_val",
    SESSION: f"{SESSION}_val",
    ACQUISITION: f"{ACQUISITION}_val",
    ANALYSIS: f"{ANALYSIS}_val",
    FILE: f"{FILE}_val",
}

log = logging.getLogger()


def test_initializer():
    em = ElementLevelMap("value", "source", "dest", "namespace")

    assert em.value == "value"
    assert em.source_data == "source"
    assert em.dest_meta == "dest"
    assert em.namespace == "namespace"


def test_info_to_dict():
    em = ElementLevelMap("value", "source", "info.middle.dest")

    output_dict = em.to_dict()
    log.info(output_dict)
    assert output_dict == {"info": {"middle": {"dest": "value"}}}


def test_add_to_dict_no_overwrite():
    em = ElementLevelMap("value", "source", "info.middle.dest", "info")

    overwritten_dict = {"info": {"middle": {"dest": "old_value"}, "extra": 5}}
    updated_overwritten = em.add_to_dict(overwritten_dict, overwrite=False)

    written_dict = {"info": {"middle": {"other": "old_value"}, "extra": 5}}
    updated_written = em.add_to_dict(written_dict, overwrite=False)

    assert updated_overwritten == {
        "info": {"middle": {"dest": "old_value"}, "extra": 5}
    }
    assert updated_written == {
        "info": {
            "middle": {"other": "old_value", "dest": "value"},
            "extra": 5,
        }
    }


def test_add_to_dict_overwrite():
    em = ElementLevelMap("value", "source", "info.middle.dest")

    overwritten_dict = {"info": {"middle": {"dest": "old_value"}, "extra": 5}}
    updated_overwritten = em.add_to_dict(overwritten_dict, overwrite=True)

    written_dict = {"info": {"middle": {"other": "old_value"}, "extra": 5}}
    updated_written = em.add_to_dict(written_dict, overwrite=True)

    assert updated_overwritten == {"info": {"middle": {"dest": "value"}, "extra": 5}}
    assert updated_written == {
        "info": {
            "middle": {"other": "old_value", "dest": "value"},
            "extra": 5,
        }
    }


def test_get_namespace():
    em = ElementLevelMap("value", "source", "info.middle.dest")
    ns = em.get_namespace()
    assert ns == "info"
