from utils.connectors.csv_connector import CSVConnector
from utils.mappers.yaml_mapper import MetadataMapper, ContainerMap, ContainerLevelMap
from utils.mappers.yaml_mapper import NAMESPACE, IMPORT, RENAME
import copy

from unittest.mock import MagicMock
from testfixtures import log_capture
import logging
log = logging.getLogger()

import pandas as pd


csv = {'group':['group1','group2'],
        'project':['project1','project2'],
        'subject':['subject1','subject2'],
        'session':['session1','session2'],
        'acquisition':['acquisition1','acquisition2'],
        'analysis':['analysis1','analysis2'],
        'file':['file1','file2'],
        'import_header_1':['meta a1','meta a2'],
        'rename_orig': ['meta b1', 'meta b2'],
       'namespace_header_1': ['meta c1','meta c2']
       }

csv_data = pd.DataFrame.from_dict(csv)
CSV_FILE = 'data.csv'
csv_data.to_csv(CSV_FILE,index = False)





def test_generic_connector_initializer():

    connector = CSVConnector("metamap", "data", "dryrun", "overwrite", "fw")
    assert connector.metamap == "metamap"
    assert connector.dryrun == "dryrun"
    assert connector.overwrite == "overwrite"
    assert connector.data == "data"
    assert connector.fw == "fw"

def test_csv_connector_load_data():
    connector = CSVConnector('metamap', fw='fw')
    connector.load_data(CSV_FILE)
    log.debug(csv_data)
    log.debug(connector.data)
    assert all(connector.data == csv_data)

@log_capture()
def test_csv_connector_process_data(capture):
    metamap = MagicMock()
    metamap.container_map.lowest_level.return_value = 'session'

    connector = CSVConnector(metamap, fw='fw')
    connector.load_data(CSV_FILE)
    connector.update_containers = MagicMock()

    connector.process_data()

    connector.metamap.container_map.lowest_level.assert_called_once()
    assert connector.update_containers.call_count == len(csv_data)
    capture.check(
        ('root', 'DEBUG', 'lowest level session has index 3')
    )


def test_csv_connector_make_meta_dict():
    VALID_KEY = "age"
    DATA_MAP = {
        IMPORT: ["import_header_1", "namespace_header_1"],
        RENAME: {"rename_orig": "rename_new"},
        NAMESPACE: {VALID_KEY: ["namespace_header_1"]},
    }

    container_level_map = ContainerLevelMap(copy.deepcopy(DATA_MAP), "session")
    current_data = csv_data.iloc[0]
    connector = CSVConnector('metamap', fw='fw')

    container = MagicMock()
    for item in container_level_map.update_items:
        setattr(container, item, {})

    update_dict = connector.make_meta_dict(container, container_level_map, current_data)

    log.debug(update_dict)
    assert len(update_dict.keys()) == len(['age', 'info'])
    assert 'age' in update_dict.keys()
    assert 'info' in update_dict.keys()
    assert update_dict['info'] == {'import_header_1': 'meta a1', 'rename_new': 'meta b1'}
    assert update_dict['age'] == 'meta c1'


def test_csv_connector_write_to_container():
    connector = CSVConnector('metamap', fw='fw')
    container = MagicMock()

    # First test with a fake file
    update_dict = {'info': 'info_val', 'race': 'race_val'}
    container.container_type = 'file'

    connector.write_to_container(container, update_dict)

    container.update_info.assert_called_with('info_val')
    container.update.assert_called_with({'race': 'race_val'})

    # Now test with a fake subject container.
    container = MagicMock()
    update_dict = {'info': 'info_val', 'race': 'race_val'}
    container.container_type = 'subject'

    connector.write_to_container(container, update_dict)

    container.update.assert_called_with({'info': 'info_val', 'race': 'race_val'})
    assert not container.update_info.called


def test_csv_connector_validate_headers_pass():
    VALID_KEY = "age"
    DATA_MAP = {
        IMPORT: ["import_header_1", "namespace_header_1"],
        RENAME: {"rename_orig": "rename_new"},
        NAMESPACE: {VALID_KEY: ["namespace_header_1"]},
    }

    mm = MetadataMapper(container_map=ContainerMap(), fw="fw")
    container_level_map = ContainerLevelMap(copy.deepcopy(DATA_MAP), "session")
    mm.set_map(container_level_map, "session")

    connector = CSVConnector(mm, fw='fw')
    connector.load_data(CSV_FILE)
    assert connector.validate_headers()

def test_csv_connector_validate_headers_fail():
    VALID_KEY = "age"
    DATA_MAP = {
        IMPORT: ["bad_header_1", "namespace_header_1"],
        RENAME: {"rename_orig": "rename_new"},
        NAMESPACE: {VALID_KEY: ["namespace_header_1"]},
    }

    mm = MetadataMapper(container_map=ContainerMap(), fw="fw")
    container_level_map = ContainerLevelMap(copy.deepcopy(DATA_MAP), "session")
    mm.set_map(container_level_map, "session")

    connector = CSVConnector(mm, fw='fw')
    connector.data = csv_data

    assert not connector.validate_headers()


# @log_capture()
#@patch('ContainerMap.get_container')
def test_csv_connector_update_containers(caplog):
    VALID_KEY = "age"
    DATA_MAP = {
        IMPORT: ["import_header_1", "namespace_header_1"],
        RENAME: {"rename_orig": "rename_new"},
        NAMESPACE: {VALID_KEY: ["namespace_header_1"]},
    }

    mm = MetadataMapper(container_map=ContainerMap(), fw="fw")
    mm.container_map.get_container = MagicMock()
    container = MagicMock()
    container.info = {}
    container.age = {}
    container.container_type = 'session'
    mm.container_map.get_container.return_value = container
    print(mm.container_map.get_container().container_type)

    container_level_map = ContainerLevelMap(copy.deepcopy(DATA_MAP), "session")
    mm.set_map(container_level_map, "session")
    fw = MagicMock()

    connector = CSVConnector(mm, fw=fw)
    connector.load_data(CSV_FILE)

    lowest_level = 'session'
    current_data = csv_data.iloc[0]
    iter_levels = ['project', 'subject', 'session']
    connector.update_containers(current_data, lowest_level, iter_levels)

     # these will help with debugin later:
    log.debug(container.call_args_list)
    log.debug(f'{container.container_type}')
    log.debug(mm.container_map.get_container.call_args_list)
    log.debug(container.update.call_args_list)
    log.debug(container_level_map.update_items)
    log.debug(container_level_map.maps)

    assert ('root', 10, 'no map for level project, skipping') in caplog.record_tuples
    assert ('root', 10, 'no map for level subject, skipping') in caplog.record_tuples
    assert ('root', 10, 'updating level session') in caplog.record_tuples
    container.update.assert_called_once()
    container.update.assert_called_with({'info': {'import_header_1': 'meta a1', 'rename_new': 'meta b1'}, 'age': 'meta c1'})




