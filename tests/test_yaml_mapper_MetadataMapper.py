from utils.mappers.yaml_mapper import MetadataMapper, ContainerMap


def test_initializer():
    mm = MetadataMapper(container_map=ContainerMap(), fw="fw")

    assert isinstance(mm.container_map, ContainerMap)
    assert mm.container_map.fw == "fw"


def test_get_map_set_map():
    mm = MetadataMapper(container_map=ContainerMap(), fw="fw")
    mm.set_map("project", "project")
    mm.set_map("subject", "subject")
    mm.set_map("session", "session")
    mm.set_map("acquisition", "acquisition")
    mm.set_map("analysis", "analysis")
    mm.set_map("file", "file")

    assert mm.get_map("project") == "project"
    assert mm.get_map("subject") == "subject"
    assert mm.get_map("session") == "session"
    assert mm.get_map("acquisition") == "acquisition"
    assert mm.get_map("analysis") == "analysis"
    assert mm.get_map("file") == "file"


def test_iter_maps():

    mm = MetadataMapper(container_map=ContainerMap(), fw="fw")
    mm.set_map("project", "project")
    mm.set_map("subject", "subject")
    mm.set_map("session", "session")
    mm.set_map("acquisition", "acquisition")
    mm.set_map("analysis", "analysis")
    mm.set_map("file", "file")

    # Ok actually I don't know how to test iter
    maps = [
        mm.project_map,
        mm.subject_map,
        mm.session_map,
        mm.acquisition_map,
        mm.analysis_map,
        mm.file_map,
    ]
    for iter_map, manual_map in zip(mm.iter_maps(), maps):
        assert iter_map == manual_map

    # Does this work? I mean it passes but...
