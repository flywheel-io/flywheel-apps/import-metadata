import logging

import sys

sys.path.append(
    "/Users/davidparker/Documents/Flywheel/SSE/MyWork/Gears/Metadata_import_Errorprone/matlab_metaimport"
)

from utils.finders.Object_Finder import FlywheelContainer

# FlywheelContainer = Object_Finder.FlywheelContainer
# fw = flywheel.Client(os.environ['FWGA_KEY'])

logging.basicConfig(level="DEBUG")
log = logging.getLogger()


def test_link_parents_success():

    # The fw client object isn't needed for this test
    group = FlywheelContainer("groupID", type="group")
    project = FlywheelContainer(None, type="project", parent=group)
    subject = FlywheelContainer("SubjectID", type="subject", parent=project)
    session = FlywheelContainer(None, type="session", parent=subject)
    acquisition = FlywheelContainer(None, type="acquisition", parent=session)
    analysis = FlywheelContainer(None, type="analysis", parent=acquisition)
    file = FlywheelContainer("FileId", type="file", parent=analysis)
    file.link_parents()

    assert file.parent.type == "subject"
    assert subject.parent.type == "group"
    assert subject.child == file


if __name__ == "__main__":
    test_link_parents_success()
