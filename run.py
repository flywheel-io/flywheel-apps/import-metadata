import logging
from pathlib import Path
import pathvalidate as pv
import sys


import flywheel
import flywheel_gear_toolkit as gt

import metadata_importer as mi


def parse_config(context):
    config = context.config
    if config["gear_log_level"] == "INFO":
        context.init_logging("info")
    else:
        context.init_logging("debug")
    context.log_config()
    log = logging.getLogger()

    return config, log


def parse_context(context):

    config, log = parse_config(context)
    metadata_file, yaml_map, fw = parse_inputs(context, log)
    return config, metadata_file, yaml_map, fw, log


def parse_inputs(context, log):

    for inp in context.config_json["inputs"].values():
        if inp["base"] == "api-key" and inp["key"]:
            api_key = inp["key"]
    fw = flywheel.Client(api_key)

    metadata_file = Path(context.get_input_path("metadata_file"))

    if metadata_file is None or not metadata_file.exists():
        log.error("No file provided or file does not exist")
        sys.exit(1)

    name = metadata_file.stem
    valid_name = pv.sanitize_filename(name)

    if len(valid_name) == 0:
        log.error(
            "You made your filename entirely out of invalid characters."
            "Just go home and think about that."
            "You are a danger to computers."
        )
        sys.exit(1)

    yaml_map = context.get_input_path("yaml_map")

    return metadata_file, yaml_map, fw


def main(context):

    # fw = flywheel.Client()

    config, metadata_file, yaml_map, fw, log = parse_context(context)
    destination = context.destination
    log.debug(destination)
    destination_id = destination['id']
    destination_object = fw.get(destination_id)

    log.info("starting main script")
    status = 0
    try:
        mi.main(
            fw,
            metadata_file,
            yaml_map=yaml_map,
            group_col=config.get("group_column"),
            project_col=config.get("project_column"),
            subject_col=config.get("subject_column"),
            session_col=config.get("session_column"),
            acquisition_col=config.get("acquisition_column"),
            analysis_col=config.get("analysis_column"),
            file_col=config.get("file_column"),
            first_row=config.get("first_row"),
            delimiter=config.get("delimiter"),
            overwrite=config.get("overwrite"),
            dry_run=config.get("dry_run"),
            destination_object=destination_object,
            rows_are_groups=config.get('rows_are_groups'),
            name_for_row_objects=config.get('name_for_row_objects')
        )
    except Exception as e:
        log.exception(e)
        status = 1

    if status > 0:
        log.info("fatal errors encountered")
    else:
        log.info('Completed successfully')

    sys.exit(status)


def jnj_test():
    import os
    fw = flywheel.Client(os.environ['JNJ_API'])
    parent_project = fw.get_project('62169aad449b8d237d1f5a1e')
    yaml_map = None
    metadata_file = '/Users/davidparker/Downloads/148_classifications.csv'
    group_col = None
    project_col = None
    subject_col = "SubID"
    session_col = None
    acquisition_col = "acq"
    analysis_col = None
    file_col = "File"
    first_row = 1
    delimiter = ','
    metadata_destination = None
    overwrite = True
    dry_run = False


    mi.main(
        fw,
        metadata_file,
        yaml_map=yaml_map,
        group_col=group_col,
        project_col=project_col,
        subject_col=subject_col,
        session_col=session_col,
        acquisition_col=acquisition_col,
        analysis_col=analysis_col,
        file_col=file_col,
        first_row=first_row,
        delimiter=delimiter,
        metadata_destination=metadata_destination,
        overwrite=overwrite,
        dry_run=dry_run,
        parent_project=parent_project
    )

def test():
    import os

    config = {
              'subject_column': 'MRN',
              'first_row':1,
              'delimiter':',',
              'overwrite': True,
              'dry_run': False,
              'rows_are_groups': False
    }

    from pathlib import Path
    yaml_map = None
    #metadata_file = Path('/Users/davidparker/Documents/Flywheel/SSE/MyWork/Gears/Metadata_import_Errorprone/matlab_metaimport/tests/instance_test/test_meta_upload - Sheet1.csv')
    metadata_file = Path('/Users/davidparker/Documents/Flywheel/Clients/public_demo/metadata_importer/Emptyupdatenotallowed/import-metadata-2.1.0_rc10_6275573d3eb8e5f30535b072/input/metadata_file/gbm-clean.csv')
    fw = flywheel.Client()
    dest = fw.get("627036ee04405d6570f92a43")
    mi.main(
        fw,
        metadata_file,
        yaml_map=yaml_map,
        group_col=config.get("group_column"),
        project_col=config.get("project_column"),
        subject_col=config.get("subject_column"),
        session_col=config.get("session_column"),
        acquisition_col=config.get("acquisition_column"),
        analysis_col=config.get("analysis_column"),
        file_col=config.get("file_column"),
        first_row=config.get("first_row"),
        delimiter=config.get("delimiter"),
        overwrite=config.get("overwrite"),
        dry_run=config.get("dry_run"),
        destination_object=dest,
        rows_are_groups=config.get('rows_are_groups'),
        name_for_row_objects=config.get('name_for_row_objects')
    )

# TODO: Make gear exit with clear warning if yaml map specifies a "container_map" header that doesn't exist

def clear_metadata():
    import os
    fw = flywheel.Client()
    project = fw.get_project()
    for session in project.sessions.iter():
        session.replace_info({})
        for acq in session.acquisitions.iter():
            acq.replace_info({})
    for subject in project.subjects.iter():
        subject.replace_info({})


if __name__ == "__main__":
    # clear_metadata()
    #test()
    #result = 0
    result = main(gt.GearToolkitContext())
    sys.exit(result)
    #test()


