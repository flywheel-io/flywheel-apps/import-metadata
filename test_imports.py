from io import BytesIO
import nibabel as nb
import matplotlib.pyplot as pl
from gzip import GzipFile
import flywheel, os

fw = flywheel.Client(os.environ["FWGA_API"])
acq = fw.get_acquisition('615ca2a0f9ee68d7c7bfd068')
raw = acq.read_file('nodif.nii.gz')

gf = GzipFile(fileobj = BytesIO(raw))
ni=nb.Nifti1Image.from_bytes(gf.read())
pl.matshow(ni.dataobj[:,:,40])

