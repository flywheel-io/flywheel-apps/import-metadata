FROM python:3.9.8-slim

ENV FLYWHEEL_SDK_REQUEST_TIMEOUT=600
ENV FLYWHEEL /flywheel/v0
RUN mkdir -p $FLYWHEEL

# Install external dependencies 
COPY requirements_base.txt /tmp/requirements.txt
RUN /usr/local/bin/python -m pip install --upgrade pip && \
    pip install -r /tmp/requirements.txt

COPY run.py $FLYWHEEL
COPY manifest.json $FLYWHEEL
COPY metadata_importer.py $FLYWHEEL
RUN mkdir $FLYWHEEL/utils && \
    mkdir $FLYWHEEL/connectors
COPY utils/* $FLYWHEEL/utils/
COPY utils/connectors/* $FLYWHEEL/connectors/


