import pandas as pd
import logging
import yaml
from utils.exceptions.loader_exceptions import InvalidMetadataFile

log = logging.getLogger(__name__)


def load_dataframe(file_path, firstrow_spec, **kwargs):
    """ Very basic method to determine file type and get it into a dataframe.  May need to be more sophisticated
        If we're going to support more than csv and xls.  Maybe a factory or something.

    Args:
        file_path (Path): a path to a file
        firstrow_spec (int): the first row that contains data (HEADERS = DATA!) in a csv or excel file
        **kwargs:

    Returns: A dataframe of metadata

    """

    if 'xls' in file_path.suffix:
        return load_excel_dataframe(file_path, firstrow_spec, sheets_spec=kwargs.get('sheets_spec', 0))
    elif 'csv' in file_path.suffix:
        return load_text_dataframe(file_path, firstrow_spec, delimiter_spec=kwargs.get('delimiter_spec', ','))
    else:
        raise InvalidMetadataFile(file_path.name)
    pass


def load_excel_dataframe(excel_path, firstrow_spec, sheets_spec):
    # First iteration only supports single sheet import, but sheet can be specified.

    df = pd.read_excel(excel_path, header=firstrow_spec - 1, sheet_name=sheets_spec)
    df = df.where(pd.notnull(df), None)

    return df


def load_text_dataframe(df_path, firstrow_spec, delimiter_spec):
    df = pd.read_table(df_path, delimiter=delimiter_spec, header=firstrow_spec - 1)
    df = df.where(pd.notnull(df), None)

    return df


def load_yaml(yaml_path):

    with open(yaml_path) as file:
        import_dict = yaml.load(file, Loader=yaml.FullLoader)

    return import_dict


def validate_df(df, object_col):

    if object_col == "" or object_col is None:
        log.info("No object column specified, assuming column 1")
        object_col = df.keys()[0]
        log.info(f"Using column {object_col}")

    if object_col not in df:
        log.error(f"Specified column {object_col} not found in CSV file")
        raise Exception("Column not in CSV")

    series = df[object_col]
    if len(series) != series.nunique():
        log.error(
            f"Non-unique object names in mapping column.  Filenames must be unique."
        )
        raise Exception("Object Mappings Must Be Unique")

    return object_col


