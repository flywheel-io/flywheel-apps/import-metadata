



class NoContainerFound(Exception):
    """Exception raised when an invalid DICOM file is encountered.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """

    def __init__(self, group=None,project=None,subject=None,session=None,acquisition=None,analysis=None,file=None):
        Exception.__init__(self)
        self.message = f"Could not locate {group=}/{project=}/{subject=}/{session=}/{acquisition=}/{analysis=}/{file=}"

    def __str__(self):
        string_out = self.message
        return string_out

class MultipleContainersFound(Exception):
    """Exception raised when an invalid DICOM file is encountered.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """

    def __init__(self, group=None,project=None,subject=None,session=None,acquisition=None,analysis=None,file=None):
        Exception.__init__(self)
        self.message = f"Multiple matches found for {group=}/{project=}/{subject=}/{session=}/{acquisition=}/{analysis=}/{file=}"

    def __str__(self):
        string_out = self.message
        return string_out