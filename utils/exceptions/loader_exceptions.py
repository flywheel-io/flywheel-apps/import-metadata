


class InvalidMetadataFile(Exception):
    """Exception raised when an invalid DICOM file is encountered.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """

    def __init__(self, file_name):
        Exception.__init__(self)
        self.message = f"Unsupported filetype for file {file_name}"

