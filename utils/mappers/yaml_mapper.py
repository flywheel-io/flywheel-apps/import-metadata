import os
import sys
import logging
import collections
import flywheel
import numpy as np
import pandas as pd
import yaml
from utils.connectors import connector_utils as cu, generic_connector as gc
from utils.exceptions.finder_exceptions import NoContainerFound, MultipleContainersFound
from pathlib import Path
from dataclasses import dataclass
import copy

sys.path.append(
    "/Users/davidparker/Documents/Flywheel/SSE/MyWork/Gears/Metadata_import_Errorprone/matlab_metaimport"
)
import utils.finders.Object_Finder as of

logging.basicConfig(level="DEBUG")
log = logging.getLogger()
log.setLevel("DEBUG")

# YAML KEYWORDS:
NAMESPACE = "namespace"
IMPORT = "import_map"
RENAME = "rename_map"
MAP = "container_map"

CONTAINER_LEVELS = [
    "group",
    "project",
    "subject",
    "session",
    "acquisition",
    "analysis",
    "file",
]


## The following are based off a yaml file with the example format:
"""
container_map:
  group: 'group_col_header'
  project: 'project_col_header'
  etc: 'etc'

<Container-Level (subject, session, etc)>:
  import_map:
    - col1
    - col2
    - col3

  rename_map:
    - Column1: new_name1
    - Column2: new_name2

  namespace:
    info.test_results:
      - col3
"""


@dataclass
class ContainerMap:
    """
    Given an single chunk of data (a row in a CSV file), the container map has info on which columns to locate
    different container labels in.

    You give the container map a row, it finds the flywheel container and returns it.

    """

    group: str = None
    project: str = None
    subject: str = None
    session: str = None
    acquisition: str = None
    analysis: str = None
    file: str = None
    fw: str = None
    link: of.FlywheelObjectLinker = of.FlywheelObjectLinker

    def lowest_level(self):
        last = [level for level in CONTAINER_LEVELS if getattr(self, level)]
        return last[-1]

    def get_container(self, current_data):

        group, project, subject, session, acquisition, analysis, file = self.get_values(
            current_data
        )
        log.debug(f"getting {group}/{project}/{subject}/{session}/{acquisition}/{file}")

        fol = self.link(
            fw=self.fw,
            group=group,
            project=project,
            subject=subject,
            session=session,
            acquisition=acquisition,
            analysis=analysis,
            file=file,
        )
        container = list(fol.find())
        if len(container) > 1:
            raise MultipleContainersFound(group=group,project=project,subject=subject,session=session,acquisition=acquisition,analysis=analysis,file=file)
        if len(container) == 0:
            raise NoContainerFound(group=group,project=project,subject=subject,session=session,acquisition=acquisition,analysis=analysis,file=file)
        container = container[0]
        return container

    def get_values(self, current_data: pd.Series):

        group = current_data.get(self.group)
        project = current_data.get(self.project)
        subject = current_data.get(self.subject)
        session = current_data.get(self.session)
        acquisition = current_data.get(self.acquisition)
        analysis = current_data.get(self.analysis)
        file = current_data.get(self.file)

        group = str(group) if group else None
        project = str(project) if project else None
        subject = str(subject) if subject else None
        session = str(session) if session else None
        acquisition = str(acquisition) if acquisition else None
        analysis = str(analysis) if analysis else None
        file = str(file) if file else None


        return group, project, subject, session, acquisition, analysis, file


@dataclass
class ContainerLevelMap:
    """
    The container level corresponds to the yaml "subject", "session", etc sections.  It represents all the metadata
    being imported for a specific container.  I.e., all columns (both from 'import' and 'map' in a given container
    level of the yaml file.
    """

    data_map: dict = None
    level: str = None
    maps: list = None
    update_items: list = None

    def __init__(self, data_map, level=None, maps=None):
        self.data_map = data_map
        self.level = level
        self.maps = maps
        self.raw = copy.deepcopy(data_map)



        self.project_namespaces = ["info", "description"]
        self.subject_namespaces = [
            "info",
            "firstname",
            "lastname",
            "age",
            "sex",
            "cohort",
            "type",
            "race",
            "ethnicity",
            "species",
            "strain",
        ]

        self.session_namespaces = [
            "operator",
            "label",
            "info",
            "project",
            "timestamp",
            "timezone",
            "subject",
            "age",
            "weight",
        ]

        self.acquisition_namespaces = ["info"]

        self.valid_namespaces = set(
            self.project_namespaces
            + self.subject_namespaces
            + self.session_namespaces
            + self.acquisition_namespaces
        )


        if data_map:
            self.validate_namespaces()
            self.validate_keys()
            self.generate_maps()
            self.get_update_items()

    def from_list(self, list):
        self.data_map = {NAMESPACE: {}, IMPORT: list, RENAME: {}}
        self.raw = {IMPORT: list, NAMESPACE: {}, RENAME:{}}
        self.validate_namespaces()
        self.validate_keys()
        self.generate_maps()
        self.get_update_items()

    def validate_namespaces(self):
        # Declare valid namespaces
        # Should this check by container type?  How would we know from the map... Perhaps that will have to be checked
        # In another part of the code idk.




        namespaces = self.data_map.get(NAMESPACE, {})

        keys = namespaces.keys()
        for key in keys:
            # first check special info case:
            if key[:5] == "info.":
                continue

            # make things lowercase.d
            if not key.islower():
                namespaces[key.lower()] = namespaces.pop(key)
                key = key.lower()

            # Check if valid
            if key not in self.valid_namespaces:
                log.error(f"namespace {key} is not valid")
                raise Exception(f"namespace {key} is not valid")
        return True

    def validate_keys(self):
        # First validate the headers that we're renaming
        remap = self.raw.get(RENAME, {})  # This is a dict
        importing = self.raw.get(IMPORT, [])  # This is a list

        # Loop through the key value pairs
        remap_pairs = remap.items()
        for old_key, new_key in remap_pairs:
            log.debug(f"{old_key} {new_key}")
            # Run the "destination" value through the safe string generator
            safe_string = cu.make_string_safe(new_key)
            if safe_string != new_key:
                # Warn if we have to change it
                log.warning(
                    f"Key {new_key} has invalid characters, changing to {safe_string}"
                )

            # If the new safe string is already in "import", we have a duplicate header problem
            if safe_string in importing:
                log.error(
                    f"Import conflict: renaming column {old_key} to {safe_string}, but {safe_string} already exists in {IMPORT} list"
                )
                raise Exception(
                    f"Duplicate Import name {safe_string} in {RENAME} and {IMPORT}"
                )
            # Reassign the old key to the safe string.  If it was already safe, this changes nothing
            remap[old_key] = safe_string

        # Remap this dictionary back to data_map[RENAME}
        self.data_map[RENAME] = remap

        # Now validate the headers we're importing
        columns_to_pop = []
        # Loop through the columns
        for column in importing:
            # Run it through the safe string maker
            safe_string = cu.make_string_safe(column)

            # if they're not the same, warn about it.
            if safe_string != column:
                log.warning(
                    f"Key {column} has invalid characters, changing to {safe_string}"
                )

                # If we had to change it, we're adding this to a remap column.
                # We'll later remove this column from "import" using pop
                self.data_map[RENAME][column] = safe_string
                columns_to_pop.append(column)

        # Kind of a weird way of popping, I know.
        self.data_map[IMPORT] = [
            c for c in self.data_map.get(IMPORT) if c not in columns_to_pop
        ]

        return True

    def generate_maps(self):
        log.debug(self.data_map)
        namespaces = self.data_map.get(NAMESPACE, {})
        columns_in_namespaces = []
        column_maps = []

        for ns, cols in namespaces.items():
            columns_in_namespaces.extend(cols)
            [self.data_map['import_map'].append(c) for c in cols if c not in self.data_map['import_map']]

        import_cols = self.data_map.get(IMPORT)
        log.debug(f"ns cols: {columns_in_namespaces}")
        for col in import_cols:
            log.debug(f"checking {col}")
            # Hi future David.  This part is a little messed up.  Namespace is intended
            # to allow access to non-info level metadata (age, cohort, etc).  But the way this
            # is implemented it would add "age.<column_header>" which is invalid.
            # But it's also useful to be able to specify namespaces like this. so idk.

            if col in columns_in_namespaces:
                log.debug(f"col {col} is in a namespace")
                ns = [n for n, items in namespaces.items() if col in items][0]
                if ns not in self.valid_namespaces:
                    dest = ns +'.'+col
                else:
                    dest = ns
            else:
                dest = "info." + col
            column_maps.append(ElementLevelMap(None, col, dest))

        rename_cols = self.data_map.get(RENAME)
        for col, meta in rename_cols.items():
            if meta in columns_in_namespaces:
                ns = [n for n, items in namespaces.items() if meta in items][0]
                dest = ns + "." + meta
            else:
                dest = "info." + meta
            column_maps.append(ElementLevelMap(None, col, dest))

        self.maps = column_maps
        log.debug(self.maps)

    def get_update_items(self):
        update_items = [m.get_namespace() for m in self.maps]
        self.update_items = set(update_items)

    def get_source_keys(self):
        keys = [m.source_data for m in self.maps]
        return keys

@dataclass
class ElementLevelMap:
    """
    This is the smallest point of data mapping.  One source to one destination.
    The Data map maps a piece of information in the INPUT data file to the flywheel metadata destination
    it has three components
    Value - the value of the metadata
    source_data: a map to where the metadata value can be found in the source data
    dest_meta: a map to where the metadata value should be written to in flywheel

    Note that these are just maps.  For example, if this is for a csv file, source_data would be a column
    header, value would be the value in that column header (for a given row), and dest_meta would be something like
    "info.meta_dest1.key1".

    This will work together with a flywheel object and the dataset to populate "value" and upload the metadata
    """

    value: "typing.Any" = None
    source_data: str = None
    dest_meta: str = None
    namespace: str = None

    def to_dict(self):
        # ns = self.get_namespace()

        layers = self.dest_meta.split(".")
        log.debug(layers)
        output_dict = self.value

        for layer in layers[::-1]:
            log.debug(f'adding {layer} to {output_dict}')
            output_dict = {str(layer): output_dict}

        # If it's namespace "info", then a further header is needed to specify
        # the nested key/value pair.  if it's not info, then we just need to set
        # the namespace to the specified value

        # if ns == "info":
        #     log.info('namespace is info')
        #     for layer in layers[::-1]:
        #         log.debug(f'adding {layer} to {output_dict}')
        #         output_dict = {str(layer): output_dict}
        # else:
        #     output_dict = {layers[0]: self.value}

        return output_dict

    def add_to_dict(self, out_dict, overwrite=False):
        update_dict = self.to_dict()
        log.debug(f"updating {out_dict} with {update_dict}")
        out_dict = self.update(out_dict, update_dict, overwrite)
        # dict = self.update(dict, update_dict, overwrite)
        return out_dict

    def get_namespace(self):
        namespace = self.dest_meta.split(".")[0]
        self.namespace = namespace
        return namespace

    def set_value(self, data):
        """gets its value from a pandas data series OR dict"""
        self.value = data[self.source_data]

    @staticmethod
    def update(original_dict, update_dict, overwrite):
        # For updating nested info metadata pieces.
        # First info must be loaded from flywheel, then updated with the new info dict
        # if not isinstance(u, dict):
        #     if len(d) > 1:
        #         log.error('Cannot update a multi-key dict with one item')
        #         raise Exception
        #     if len(d) == 0:
        #
        #     key=list(d.keys())[0]
        #     d[key] = u
        #     return d

        for key, val in update_dict.items():
            if isinstance(val, collections.abc.Mapping):
                original_dict[key] = ElementLevelMap.update(original_dict.get(key, {}), val, overwrite)
            else:
                # Flywheel doesn't like numpy data types:
                if type(val).__module__ == np.__name__:
                    val = val.item()

                log.debug(f'checking if "{key}" in {original_dict.keys()}')
                if key in original_dict:
                    if original_dict[key] in [None, {}, '', []]:
                        log.debug(f"key '{key}' is blank, replacing")
                        original_dict[key] = val

                    if overwrite:
                        log.debug(f'Overwriting "{key}" from "{original_dict[key]}" to "{val}"')
                        original_dict[key] = val
                    else:
                        log.debug(f'"{key}" present.  Skipping.')
                else:
                    log.debug(f"setting {key}")
                    original_dict[key] = val

        return original_dict


class YamlLoader:
    """
    Responsibilities:
        - loading the yaml file,
        - converting to python dict,
        - ensuring safe names
    """

    def __init__(self, data_map={}):

        self.data_map = data_map
        self.container_map = ContainerMap()
        self.MetaMaps = []

        # self.source_of_truth = "data"
        # self.reset = False
        # self.update_existing = True
        # self.add_new = True

    def load_map(self):
        """
        Loads the yaml map.
        Belongs with the mapper class
        """

        if isinstance(self.data_map, dict):
            log.info("Mapping dictionary recognized")
            return

        if self.data_map is None:
            return

        if isinstance(self.data_map, str):
            self.data_map = Path(self.data_map)

        if Path.is_file(self.data_map):
            if self.data_map.suffix != ".yaml":
                raise TypeError(
                    "data_map must be of type dict or a yaml file ending in '.yaml'"
                )

            log.info("yaml file recognized for map.  Loading.")
            with open(self.data_map) as file:
                self.data_map = yaml.load(file, Loader=yaml.FullLoader)
            self.find_object_locator_columns()

        return

    def find_object_locator_columns(self):

        self.container_info = self.data_map.pop(MAP, {})

        if not self.container_info:
            log.warning(
                f"section '{MAP}' not found in yaml.  Assuming fw container names are under columns 'group','project','subject',etc..."
            )

            self.container_info = {
                "group": "group",
                "project": "project",
                "subject": "subject",
                "session": "session",
                "acquisition": "acquisition",
                "analysis": "analysis",
                "file": "file",
            }

        self.container_map = ContainerMap(**self.container_info)
        pass

    def generate_metamapper(self, fw):
        metamap = MetadataMapper(self.container_map, fw)

        levels = CONTAINER_LEVELS[1:]  # Leave off the group

        for level in levels:
            mp = self.data_map.get(level)
            if mp:
                containerlevelmap = ContainerLevelMap(mp, level)
                metamap.set_map(containerlevelmap, level)

        return metamap

    def generate_connector(self, fw, data, dryrun=False, overwrite=True):
        metamapper = self.generate_metamapper(fw)
        connector = gc.CSVConnector(
            metamapper, data, dryrun=dryrun, overwrite=overwrite
        )
        return connector

    def generate_default_mapper(self, fw, headers=[], container_mappings={}, namespace=""):
        my_map = {}

        for level in CONTAINER_LEVELS:
            my_map[level] = container_mappings.get(level) if container_mappings else level

        self.container_map = ContainerMap(fw=fw, **my_map)
        metamap = MetadataMapper(self.container_map, fw)

        # Remove header keys from header
        for val in container_mappings.values():
            if val in headers:
                headers.remove(val)

        lowest_level = self.container_map.lowest_level()
        if namespace:
            raw_data_map = {NAMESPACE: {f'info.{namespace}': list(headers)}, IMPORT: [], RENAME: {}}
            my_container_map = ContainerLevelMap(
                data_map=raw_data_map, level=lowest_level, maps=None
            )
        else:
            my_container_map = ContainerLevelMap(
                data_map=None, level=lowest_level, maps=None
            )
            my_container_map.from_list(list(headers))


        metamap.set_map(my_container_map, lowest_level)
        return metamap


class MetadataMapper:
    def __init__(self, container_map=None, fw=None):
        self.container_map = container_map
        self.container_map.fw = fw
        self.project_map = None
        self.subject_map = None
        self.session_map = None
        self.acquisition_map = None
        self.analysis_map = None
        self.file_map = None

    def get_map(self, level):
        return getattr(self, f"{level}_map")

    def set_map(self, containerlevelmap, level):
        setattr(self, f"{level}_map", containerlevelmap)

    def iter_maps(self):
        maps = [self.project_map, self.subject_map, self.session_map, self.acquisition_map, self.analysis_map, self.file_map]
        for container_map in maps:
            if container_map:
                yield container_map




def test_MetadataMap_NoDataMap_NoNamespace():
    import flywheel
    import os

    fw = flywheel.Client(os.environ["FWGA_API"])
    target_acq = fw.get_acquisition("6053d2f50858d11c7782d35e")
    # get to it here:
    # https://ga.ce.flywheel.io/#/projects/603d3ab6357432fe34a8318d/sessions/6053d2f40858d11c7782d35d?tab=data
    sample_data = {
        "Nest1": {"Nested_Key1": "Nested_Value1", "Nested_Key2": True},
        "Key1": 1,
        "Key2": "Val2",
    }

    my_map = MetadataMapper(
        fw, data=sample_data, fw_object=target_acq, data_map=None, namespace=""
    )
    my_map.write_metadata()


def test_MetadataMap_NoDataMap_Namespace():
    import flywheel
    import os

    fw = flywheel.Client(os.environ["FWGA_API"])
    target_acq = fw.get_acquisition("6053d2f50858d11c7782d35e")
    # get to it here:
    # https://ga.ce.flywheel.io/#/projects/603d3ab6357432fe34a8318d/sessions/6053d2f40858d11c7782d35d?tab=data
    sample_data = {
        "Nest1": {"Nested_Key1": "Nested_Value1", "Nested_Key2": True},
        "Key1": 1,
        "Key2": "Val2",
    }

    my_map = MetadataMapper(
        fw, data=sample_data, fw_object=target_acq, data_map=None, namespace="Namespace"
    )
    my_map.write_metadata()


def test_MetadataMap_DataMap_NoNamespace():
    import flywheel
    import os

    fw = flywheel.Client(os.environ["FWGA_API"])
    target_acq = fw.get_acquisition("6053d2f50858d11c7782d35e")
    # get to it here:
    # https://ga.ce.flywheel.io/#/projects/603d3ab6357432fe34a8318d/sessions/6053d2f40858d11c7782d35d?tab=data
    sample_data = {
        "Nest1": {"Nested_Key1": "Nested_Value1", "Nested_Key2": True},
        "Key1": 1,
        "Key2": "Val2",
        "Key3": "Val3",
        "Key4": "Val4",
        "Key5": 5,
    }
    map_path = "/Users/davidparker/Documents/Flywheel/SSE/MyWork/Gears/Metadata_import_Errorprone/matlab_metaimport/tests/test_mapper.yaml"

    my_map = MetadataMapper(
        fw, data=sample_data, fw_object=target_acq, data_map=map_path, namespace=""
    )
    my_map.write_metadata()


def test_MetadataMap_DataMap_Namespace():
    import flywheel
    import os

    fw = flywheel.Client(os.environ["FWGA_API"])
    target_acq = fw.get_acquisition("6053d2f50858d11c7782d35e")
    # get to it here:
    # https://ga.ce.flywheel.io/#/projects/603d3ab6357432fe34a8318d/sessions/6053d2f40858d11c7782d35d?tab=data
    sample_data = {
        "Nest1": {"Nested_Key1": "Nested_Value1", "Nested_Key2": True},
        "Key1": 1,
        "Key2": "Val2",
        "Key3": "Val3",
        "Key4": "Val4",
        "Key5": 5,
    }
    map_path = "/Users/davidparker/Documents/Flywheel/SSE/MyWork/Gears/Metadata_import_Errorprone/matlab_metaimport/tests/test_mapper.yaml"

    my_map = MetadataMapper(
        fw,
        data=sample_data,
        fw_object=target_acq,
        data_map=map_path,
        namespace="MainSpace",
    )
    my_map.write_metadata()


def clear_acq():
    import flywheel
    import os

    fw = flywheel.Client(os.environ["FWGA_API"])
    target_acq = fw.get_acquisition("6053d2f50858d11c7782d35e")

    target_acq.replace_info({})


def test_current():

    fw = flywheel.Client(os.environ["FWGA_API"])

    yaml_file = "/Users/davidparker/Documents/Flywheel/SSE/MyWork/Gears/Metadata_import_Errorprone/matlab_metaimport/tests/instance_test/example_map.yaml"
    data = pd.read_csv(
        "/Users/davidparker/Documents/Flywheel/SSE/MyWork/Gears/Metadata_import_Errorprone/matlab_metaimport/tests/instance_test/test_meta_upload - Sheet1.csv"
    )
    raw_map = YamlLoader(yaml_file)
    raw_map.load_map()
    metamapper = raw_map.generate_metamapper(fw)
    print(metamapper.subject_map.update_items)
    connector = gc.CSVConnector(metamapper, data, dryrun=False, overwrite=True)
    connector.process_data()


if __name__ == "__main__":
    test_current()

