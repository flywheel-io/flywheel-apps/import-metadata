# Object_Finder.py

## Purpose

The object_finder.py file houses the `FlywheelObjectFinder` class.  The intention of this
class is to find flywheel objects given ***any*** amount of information.  For example:

1. A user knows that within a given project, filenames at the acquisition level are 
unique.  So to find the file "unique_file.txt" at the acquisition level, 