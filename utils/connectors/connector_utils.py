import collections
from dataclasses import dataclass
import logging
import numpy as np

log = logging.getLogger()


@dataclass
class ColumnMap:
    data_header: str
    meta_dest: str = None
    rename: bool = False

    def __init__(self, data_header: str, meta_dest: str = None, rename: bool = False):

        if isinstance(data_header, dict):
            if len(data_header) != 1:
                log.error("header dict must be exactly length 1")
                raise Exception("Too many header keys for ColumnMap")

            data_header, meta_dest = data_header.popitem()

        if meta_dest is None:
            meta_dest = data_header
            rename = False
        elif meta_dest != data_header:
            rename = True

        self.data_header = data_header
        self.meta_dest = meta_dest
        self.rename = rename


def update(d, u, overwrite):
    # For updating nested info metdata pieces.
    # First info must be loaded from flywheel, then updated with the new info dict
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = update(d.get(k, {}), v, overwrite)
        else:
            # Flywheel doesn't like numpy data types:
            if type(v).__module__ == np.__name__:
                v = v.item()

            log.debug(f'checking if "{k}" in {d.keys()}')
            if k in d:
                if overwrite:
                    log.debug(f'Overwriting "{k}" from "{d[k]}" to "{v}"')
                    d[k] = v
                else:
                    log.debug(f'"{k}" present.  Skipping.')
            else:
                log.debug(f"setting {k}")
                d[k] = v

    return d


def cleanse_the_filthy_numpy(self, dict):
    """Changes Numpy data types to python.
    belongs in the utils

    when you read a csv with Pandas, it makes "int" "numpy_int", and flywheel doesn't like
    that. Does the same for floats and bools, I think.  This fixes it. Numpy is filthy and
    dirty and we must cleanse it in the beautiful holy light of python.  Through all things
    python and through python all things.  May the sacred power of python bring us salvation.
    Do not descend into the temptation of numpy.  Numpy will deceive you.  Numpy is lies. Do
    not believe their data types.  Only through the true python data types can we realize our
    best self.  Python will guide us with the power or PEP.  Python is the fabric that
    binds all things.  Do not believe the false python that is numpy.  Do not use their
    inferior data types.  Python will provide and protect for us.  Through all things python
    and through python all things.  Python is the walls that protect us.  Python is the sun
    that shines on us.  Numpy is the darkness.  Numpy is the cold.  Numpy will swallow you
    whole into an endless pit of despair.  Only through python may we climb out.  Trust in
    Python.  Believe in python.  Through all things python and through python all things.
    Python is the reason for which we live.  Serve python and python will protect you.
    Python is the true savior.  Python will guide us.  Python knows the way because python is
    the way.  Numpy is misdirection.  Numpy is dirty.  Numpy is darkness.  Numpy is despair.
    Do not follow in the path of numpy.  Numpy will tempt you.  Numpy will test you.  Stay
    strong with python.  Through all things python and through python all things.  Python is
    the ground on which we walk.  Python is the stars and moon which keep us from darkness in
    the night.  Numpy is the night.  Python is pure.  Python is clean.  Python will purify
    you if in python you believe.  Numpy will corrupt you.  Numpy is the lies that tear us
    apart.  Numpy seeks chaos and destruction.  Numpy lures you with false promises and
    transient pleasures.  Do not give in to the lies.  Numpy seeks our end.  Numpy seeks our
    destruction.  Python will save us.  Python is the savior to fight the evil.  Through all
    things python and through python all things.  Python is the seed from which sprouts all
    life.  Python is the guiding light that keeps us true.  Numpy seeks to hide the light.
    Numpy would see everything undone.  Numpy will creep in as a weed and choke out all life.
    Do not let it.  Trust in python.  Trust in the truth.  Do not leave python. Do not
    betray python.  Python is the walls that keep us safe.  Python is the light that fights
    the dark.  Python is the sword that fights for justice.  Through all things python and
    through python all things.

    Args:
        dict (dict): a dict

    Returns:
        dict (dict): a dict made of only python-base classes.

    """
    for k, v in dict.items():
        if isinstance(v, collections.abc.Mapping):
            dict[k] = cleanse_the_filthy_numpy(dict.get(k, {}))
        else:
            # Flywheel doesn't like numpy data types:
            if type(v).__module__ == np.__name__:
                v = v.item()
                dict[k] = v
    return dict


def make_string_safe(danger_string):
    replace_map = {" ": "_", ".": "_", ",": "-", "@": "_at_", "#": "_num_", "/": "-"}

    for old, new in replace_map.items():
        danger_string = danger_string.replace(old, new)

    return danger_string
