import logging
import pandas as pd
from utils.connectors.generic_connector import Connector

log = logging.getLogger()

# Maybe the json file needs to be exploded into a dataframe so that it can match the json file?

class JSONConnector(Connector):
    def load_data(self, data_path):
        self.data = pd.read_json(data_path)

    def preprocess_data(self, data):
        # Performs any preprocessing on the data that's necessary.
        pass

    def make_meta_dict(self, container, current_map, current_data):
        """
        Makes a metadata dictionary from the imported data.
        The metadata dictionary is what gets written to flywheel
        belongs in the connector class
        Is this for the csv subclass or the generic parent class?
        """

        update_items = current_map.update_items
        maps = current_map.maps

        update_dict = {}
        for item in update_items:
            update_dict[item] = getattr(container, item, None)

        for element in maps:
            element.set_value(current_data)
            element.add_to_dict(update_dict, self.overwrite)

        return update_dict

    def process_data(self):

        lowest_level = self.metamap.container_map.lowest_level()
        levels = ["file", "analysis", "acquisition", "session", "subject", "project"]
        idx = [i for i, v in enumerate(levels) if v == lowest_level][0]
        log.debug(f"lowest level {lowest_level} has index {idx}")
        iter_levels = levels[idx:]

        for index, row in self.data.iterrows():
            self.update_containers(row, lowest_level, iter_levels)

        pass
