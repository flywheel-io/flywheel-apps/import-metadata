from utils.finders import Container_Finder
import logging

log = logging.getLogger()


class GenericWriter:
    """
    Responsible for - writing a metadata map to flywheel.  iterates through data, uses the keys for finding objects
    in the data, uses Container_finder to find
    """

    def __init__(self, fw, container_mappings, data, import_values=None, dryrun=False):
        self.container_mappings = container_mappings
        self.import_values = import_values
        self.dryrun = dryrun
        self.data = data
        self.fw = fw
        self.rows_are_objects

    def get_container_info_from_row(self, row):

        container_map = {key: str(row[val]) for key, val in self.container_mappings.items()}

        container_finder = Container_Finder.FlywheelObjectLinker(
            self.fw, **container_map
        )
        results = container_finder.find()

        if len(results) > 1:
            log.error(f"More than one result for the following search: {container_map}")
            raise Exception("Too many containers match provided parameters")
        elif len(results) == 0:
            log.warning(f"no results found for search {container_map}, skipping")
            return

        result = results[0]

    def write_metadata(self, fw_object, overwrite=False, method="update"):
        """
        Writes metadata.
        Belongs with the writer class
        """

        if method == "update":
            pass

        update_dict = self.make_meta_dict()
        object_info = fw_object.get("info")
        if not object_info:
            object_info = dict()

        print(object_info)
        object_info = self.update(object_info, update_dict, overwrite)
        print(object_info)
        fw_object.update_info(object_info)

