from abc import ABC, abstractmethod
import collections
import logging
import pandas as pd
import yaml
from utils.exceptions.finder_exceptions import NoContainerFound, MultipleContainersFound

log = logging.getLogger()


class Connector(ABC):
    """
    Responsible for - loading data and mapping it to flywheel metadata endpoints
    """

    def __init__(self, metamap, data=None, dryrun=False, overwrite=False, fw=None):
        self.metamap = metamap
        self.dryrun = dryrun
        self.overwrite = overwrite
        self.data = data
        if not fw:
            self.fw = metamap.container_map.fw
        else:
            self.fw = fw

    @abstractmethod
    def load_data(self, data_path):
        pass

    @abstractmethod
    def preprocess_data(self, data):
        # Performs any preprocessing on the data that's necessary.
        pass

    @abstractmethod
    def process_data(self):
        pass

    @abstractmethod
    def make_meta_dict(self, container, current_map, current_data):
        """
        Makes a metadata dictionary from the imported data.
        The metadata dictionary is what gets written to flywheel
        belongs in the connector class
        """
        update_dict = {}
        return update_dict

    @staticmethod
    def write_to_container(container, update_dict):
        log.debug("updating...")
        if container.container_type == 'file':
            if 'info' in update_dict.keys():
                info = update_dict.pop('info')
                container.update_info(info)
                if update_dict:
                    container.update(update_dict)
        else:
            container.update(update_dict)

    def validate_headers(self):

        data_keys = self.data.keys()

        for container_map in self.metamap.iter_maps():
            header_keys = container_map.get_source_keys()

            not_present = [k for k in header_keys if k not in data_keys]

            if not_present:
                log.error(f'keys {not_present} are not present in the data headers')
                return False

        return True



    def update_containers(self, current_data, lowest_level, iter_levels):

        try:
            container = self.metamap.container_map.get_container(current_data)
        except MultipleContainersFound as e:
            log.exception(e)
            log.info('skipping')
            return
        except NoContainerFound as e:
            log.exception(e)
            log.info('skipping')
            return


        #log.debug(container)
        log.debug(f"level is {container.container_type}")

        for level in iter_levels:
            update_dict = {}
            current_map = self.metamap.get_map(level)

            if not current_map:
                log.debug(f"no map for level {level}, skipping")
                continue

            log.debug(f"updating level {level}")


            if level != lowest_level:
                log.debug(
                    f"{level} is not lowest level {lowest_level}, getting parent finder"
                )
                finder = getattr(self.fw, f"get_{level}")
                new_id = container.parents.get(level)
                log.debug(f"looking for id {new_id}")
                if not new_id:
                    if self.metamap.get_level(level) is not None:
                        log.warning(
                            f"Warning, level {level} specified but {container.container_type} has no parent of that type"
                        )
                    continue
                container = finder(new_id)
            log.debug("updating map")


            update_dict = self.make_meta_dict(container, current_map, current_data)
            self.write_to_container(container, update_dict)


class TestData:
    def __init__(self):
        data = {
            "col1": [1, 2, 3, 4],
            "col2": ["a", "b", "c", "d"],
            "col3": ["one", "two", "three", "four"],
            "Column1": [10, 20, 20, 40],
            "Column2": ["e", "f", "g", "h"],
        }
