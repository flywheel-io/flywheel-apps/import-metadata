import logging
import pandas as pd
from utils.connectors.generic_connector import Connector

log = logging.getLogger()


class CSVConnector(Connector):
    def load_data(self, data_path):
        self.data = pd.read_csv(data_path)

    def preprocess_data(self, data):
        # Performs any preprocessing on the data that's necessary.
        pass

    def make_meta_dict(self, container, current_map, current_data):
        """
        Makes a metadata dictionary from the imported data.
        The metadata dictionary is what gets written to flywheel
        belongs in the connector class
        Is this for the csv subclass or the generic parent class?
        """

        update_items = current_map.update_items
        maps = current_map.maps

        update_dict = {}
        for item in update_items:
            update_dict[item] = getattr(container, item, None)

        for element in maps:
            element.set_value(current_data)
            update_dict = element.add_to_dict(update_dict, self.overwrite)

        return update_dict

    def process_data(self):

        lowest_level = self.metamap.container_map.lowest_level()
        levels = ["file", "analysis", "acquisition", "session", "subject", "project"]
        idx = [i for i, v in enumerate(levels) if v == lowest_level][0]
        log.debug(f"lowest level {lowest_level} has index {idx}")
        iter_levels = levels[idx:]

        for index, row in self.data.iterrows():
            try:
                self.update_containers(row, lowest_level, iter_levels)
            except Exception as e:
                log.exception(e)


        pass

    def rows_are_groups(self, parent_key):

        lowest_level = self.metamap.container_map.lowest_level()
        levels = ["file", "analysis", "acquisition", "session", "subject", "project", "group"]
        idx = [i for i, v in enumerate(levels) if v == lowest_level][0]
        log.debug(f"lowest level {lowest_level} has index {idx}")
        iter_levels = levels[idx:]

        groupby = [getattr(self.metamap.container_map, level) for level in iter_levels[::-1] if getattr(self.metamap.container_map, level)]

        grouped_df = self.data.groupby(groupby)

        new_df = groups_to_list(grouped_df, parent_key)
        self.data = new_df



def groups_to_list(grouped_df, parent_key):

    new_columns = grouped_df.keys.copy()
    new_columns.append(parent_key)
    new_df = pd.DataFrame(columns=new_columns)

    for group_key in grouped_df.groups:
        group = grouped_df.get_group(group_key)
        group = group.drop(labels=grouped_df.keys, axis=1)

        new_value = []
        for index, row in group.iterrows():
            new_value.append(row.to_dict())

        packed_values = list(group_key)
        packed_values.append(new_value)

        new_df_row = {k:v for k,v in zip(new_columns, packed_values)}
        new_df = new_df.append(new_df_row, ignore_index=True)

    return new_df






