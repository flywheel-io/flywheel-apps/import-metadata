import logging
import flywheel
import types

import utils.finders.Container_Finder as cf

logging.basicConfig(level="DEBUG")
log = logging.getLogger()


def print_container(c):
    ctype = getattr(c, "container_type", "file")
    if ctype == "file":
        name_key = "name"
    else:
        name_key = "label"

    print(
        f"label: {getattr(c,name_key)}\n"
        f"type: {c.container_type}\n"
        f"parents: {c.parents}"
    )


class FlywheelObjectLinker:
    def __init__(
        self,
        fw,
        group=None,
        project=None,
        subject=None,
        session=None,
        acquisition=None,
        analysis=None,
        file=None,
        level=None,
    ):

        log.debug(f"group: {group}")
        log.debug(f"project: {project}")
        log.debug(f"subject: {subject}")
        log.debug(f"session: {session}")
        log.debug(f"acquisition: {acquisition}")
        log.debug(f"analysis: {analysis}")
        log.debug(f"file: {file}")
        log.debug(f"level: {level}")


        self.client = fw

        self.order = [
            "group",
            "project",
            "subject",
            "session",
            "acquisition",
            "analysis",
        ]
        self.CONTAINER_ID_FORMAT = "^[0-9a-fA-F]{24}$"
        self.level = level  # The level that the object we're looking for is
        self.object = None
        self.highest_container = None
        self.lowest_level = None
        self.highest_level = None
        self.lowest_provided_level = None

        # This tracks if the file/analysis we're looking for is on the lowest PROVIDED level (provided with an ID/label)
        self.list_on_lowest = False

        self.group = group
        self.project = project
        self.subject = subject
        self.session = session
        self.acquisition = acquisition
        self.analysis = analysis
        self.file = file

        self.find_levels()
        self.found_objects = None

    def __str__(self):
        """Defines the print function of the class

        Prints a summary of the flywheel object being located in the form of a Flywheel
        path

        """
        string_out = ""
        for key, val in self.__dict__.items():
            if key in [
                "file",
                "analysis",
                "acquisition",
                "session",
                "subject",
                "project",
                "group",
            ]:
                string_out += f"{key}: {val.get('id')}\n"
            else:
                string_out += f"{key}: {val}\n"

        given_path = f"{self.group['id']}/{self.project['id']}/{self.subject['id']}/{self.session['id']}/{self.acquisition['id']}/{self.analysis['id']}/{self.file['id']}"
        string_out += f"Given Path: {given_path}"

        return string_out

    def from_mapper(self, mapper):
        self.group = mapper.GROUP
        self.project = mapper.PROJECT
        self.subject = mapper.subject
        self.session = mapper.session
        self.acquisition = mapper.acquisition
        self.analysis = mapper.analysis
        self.file = mapper.file

        self.find_levels()
        self.found_objects = None

    def validate_lowest_level(self):

        lowest_check = self.lowest_level

        print(lowest_check)
        # Perform check to make sure that no levels lower than "level" have
        # been specified:
        if self.level is None:
            self.level = lowest_check

        check_order = self.order
        check_order.append("file")

        my_level = [i for i, v in enumerate(check_order) if v == self.level][0]
        found_level = [i for i, v in enumerate(check_order) if v == lowest_check][0]

        if my_level > found_level:
            log.error(
                f"File is specified on level {self.level}, but user has "
                f"Specified containers of type {self.lowest_level}, which is a child"
            )
            raise Exception("Lowest level found is child of specified file level")

        log.debug(f"lowest level: '{self.lowest_level}' is valid")

    def validate_specified_level(self):
        # Check to see if level is given and if it's a valid name:
        if self.level is not None and self.level not in self.order:
            log.warning(f"level {self.level} is not valid")
            raise Exception(f"Invalid level: {self.level}")

        log.debug(f"specified level: '{self.level}' is valid")

    def get_highest_level(self):
        for o in self.order[::-1]:
            test_case = getattr(self, o)
            if test_case is not None:
                self.highest_level = o

    def get_lowest_level(self):
        for o in self.order[:-1]:
            test_case = getattr(self, o)
            if test_case is not None:
                self.lowest_level = o

    def get_highest_level_container(self):
        if self.highest_level == "group":
            container = self.client.get_group(self.group)
            return container

        container_finder = cf.ContainerFinder(None, self.client)
        label = getattr(self, self.highest_level)
        is_id = container_finder.is_fwid(label)
        results = container_finder.run_client_query(self.highest_level, label, is_id)

        container = None
        if isinstance(results, types.GeneratorType) or isinstance(results, list):
            for ir, r in enumerate(results):
                container = r.reload()

                if ir > 0:
                    log.error(
                        f"multiple results found for specified level {self.highest_level} and label {label}"
                    )
                    raise Exception(
                        f"too many possible containers for {self.highest_level} and label {label} without additional constraints"
                    )
        else:
            container = results

        if container is None:
            log.error(
                f"no results found for specified level {self.highest_level} and label {label}"
            )
            raise Exception(
                f"no matching containers for {self.highest_level} and label {label}"
            )

        return container

    def check_for_files_and_analyses(self):

        # Case 1: A file is given, and a level is not:
        if self.file is not None and self.level is None:
            # Set the "level" to the lowest_level. and set "lowest_level" to file
            self.level = self.lowest_level
            self.lowest_level = "file"
            self.list_on_lowest = True

        # Case 2: A file is given, and a level is also given:
        # ex) search for file "text.txt" at the acquistiion level on project PROJ, subject SUBJ
        elif self.file is not None and self.level is not None:
            # Set the lowest level to "file"
            self.lowest_provided_level = self.lowest_level
            self.lowest_level = "file"
            self.list_on_lowest = False

        # Case 3: an analysis is given and a level is not:
        elif self.analysis is not None and self.level is None:
            # Set the "level" to the lowest_level. and set "lowest_level" to file
            self.level = self.lowest_level
            self.lowest_level = "analysis"
            self.list_on_lowest = True

        # Case 4: an analysis is given and a level is also given:, in theory "lowest level"
        # will already be analysis, so this is unnecessary, but I'm including it to be complete
        # in case things change in the future, idk.
        elif self.analysis is not None and self.level is not None:
            # Set the lowest level to "analysis"
            self.lowest_provided_level = self.lowest_level
            self.lowest_level = "analysis"

    def find_levels(self):
        """Finds the highest and lowest flywheel object levels given

        This performs most of the logic that allows for a relatively simple
        orchestration function "process_matches()"

        a level is considered "given" if that level has a specified label to use for
        a query.

        This is later used to determine where to start searching for the specified item.
        For example, if the highest level given is "Subject", we will start our search
        at `flywheel.subjects.find()`, as there's nothing specifying which projects we
        should use, meaning we just use them all.

        The lowest level is where we stop running searches at.  If the highest level is
        "project" and the lowest level is "acquisition", this will tell us to search for
        all acquisitions that match the provided acquisition label, and to only run this
        search on all subjects within the projects that match the provided project
        label.

        After determining what the lowest and highest levels are, this function
        checks to see if the lowest level is a file or analysis.  If it is, it then
        checks to see if the `self.level` property has a value.  If `self.level` is None
        then the code assumes that the file or analysis is attached to the lowest level
        provided.  HOWEVER, if `self.level` has a value, we will look for the file or
        analysis at that level.

        EXAMPLE:
            # A user passes in a project ID, a subject ID, a file name, and sets 'level'
            # To "acquisition":

            finder = FlywheelObjectFinder(fw,
                                          project="Project",
                                          subject="Subject",
                                          file="file_name",
                                          level="acquisition")

            # finder.lowest_level  will be "acquisition"
            # and finder.highest_level will be "Project"

            # since the lowest level provided is "file", the finder will be looking for
            # a file.  Because we provided a subject, the finder will only be looking
            # for this file on subjects that match the label/id "Subject".  Because we
            # provided a project, the finder will only be looking for subjects/files
            # in a project that matches the label/id "Project".
            # We also provide the "level" property.  IF LEVEL WAS NOT PASSED, the finder
            # would assume that the file is attached to the next 'lowest' level
            # provided, which in this case would be `subject`.  HOWEVER, because we pass
            # in a "level", this implies that we are looking for files named `file_name`
            # on an acquisition in a subject named `subject` in a project named
            # `project`.  This means that the finder will have to search all
            # acquisitions on the matching subject to try to find it.




        """

        # File is not included in this because it's a separate check because if a file
        # is given and the "level" property is not set, we'll set it to the next lowest
        # level, which will be found here.
        # self.order = ["group", "project", "subject", "session", "acquisition", "analysis"]
        self.validate_specified_level()

        # Check for the highest level provided (highest in the hierarchy described in 'order')
        self.get_highest_level()

        # Now get the lowest
        self.get_lowest_level()

        # Now we need to check and see if a file name or analysis name is provided.
        # If it is, we will assume that it is attached to the lowest level of actual
        # container provided.  HOWEVER, if the `self.level` value is specified, then
        # this implies that they will be stored on that level.
        self.check_for_files_and_analyses()

        # Validate that our lowest level isn't TOO LOW
        self.validate_lowest_level()

        log.info(f"highest level is {self.highest_level}")
        log.debug(f"lowest level is {self.lowest_level}")

    def find(self, current=None):

        if not current:
            log.debug(f"fresh search, starting with {self.highest_level}")
            highest_container = self.get_highest_level_container()
            current = highest_container
            current_type = self.highest_level
        else:
            log.debug(
                f'ongoing. Looking at level {getattr(current,"container_type",None)}'
            )
            current_type = getattr(current, "container_type", "file")

        highest_index = [i for i, o in enumerate(self.order) if o == current_type][0]
        new_order = self.order[highest_index + 1 :]
        order_dict = {value: index for index, value in enumerate(new_order)}
        log.debug(f"new order: {new_order}")

        if len(new_order) < 1:
            log.debug("end of the line")
            return []

        for level in new_order:

            if getattr(self, level) is None:
                log.debug(f"level {level} is none")

                if self.level == level:
                    log.debug(
                        "but this is the level we're searching for. should be analysis or file"
                    )
                    container_finder = cf.ContainerFinder(current, self.client)
                    # Pass query = None to get all containers at level
                    containers_to_search = container_finder.find(level, None)

                    results = []
                    for container in containers_to_search:
                        results.extend(list(self.find(container.reload())))
                    return results

                if order_dict[level] < order_dict[self.lowest_level]:
                    log.debug("and it is not the lowest level.  skipping")
                    continue
                else:
                    log.debug("some error got you lower than lowest")
                    log.debug(
                        f"lowest level is {self.lowest_level}, {order_dict[self.lowest_level]}"
                    )
                    log.debug(f"current level is {level}, {order_dict[level]}")

            elif level == self.lowest_level:
                log.debug(f"at the lowest level {level}")
                container_finder = cf.ContainerFinder(current, self.client)
                log.debug(
                    f"searching for {getattr(self, level)} at level {level} from level {current_type}"
                )
                results = container_finder.find(level, getattr(self, level))
                return results

            else:
                log.debug("mid-level search")
                container_finder = cf.ContainerFinder(current, self.client)
                log.debug(
                    f"searching for {getattr(self, level)} at level {level} from level {current_type}"
                )
                results = container_finder.find(level, getattr(self, level))
                full_results = []
                log.debug("descending")
                for r in results:
                    #log.debug(r)
                    full_results.extend(list(self.find(r.reload())))

                return full_results

    def generate_path_to_container(self, parent_container=None):
        """Builds a human readable path to a flywheel container

        Once an object has been located by the finder, this function generates a
        Flywheel path to it. Or, a container object can be passed in, and a path
        will be generated from that.

        Args:
            parent_container (flywheel.ContainerReference): A Flywheel container to
            generate a flywheel path from.  If not present, will attempt to generate
            a path for any found objects that match the search criterea.

        Returns:
            fw_paths (list): a list of strings to flywheel paths.

        """

        fw = self.client
        fw_paths = []

        if parent_container is None:
            log.debug("parent container is None, using self.found_objects:")
            log.debug(f"found {len(self.found_objects)} objects")
            containers_to_loop = self.found_objects
        else:
            log.debug(f"parent container is present. type {type(parent_container)} ")
            containers_to_loop = [parent_container]

        for container in containers_to_loop:
            try:
                ct = container.container_type
                log.debug(f"container type is {ct}")
            except Exception:
                log.debug("container type is assumed to be analysis")
                ct = "analysis"

            if ct == "file":
                path_to_file = self.generate_path_to_container(
                    container.parent.reload()
                )
                path_to_file = path_to_file[0]
                fw_path = f"{path_to_file}/{container.name}"

            else:
                fw_path = ""

                if container.parents.GROUP is not None:
                    append = container.parents.GROUP
                else:
                    append = ""

                fw_path += append

                if container.parents.PROJECT is not None:
                    project = self.get_project(container)
                    append = f"/{project.label}"
                else:
                    append = ""

                fw_path += append

                if container.parents.subject is not None:
                    subject = self.get_subject(container)[0]
                    append = f"/{subject.label}"
                else:
                    append = ""

                fw_path += append

                if container.parents.session is not None:
                    session = self.get_session(container)[0]
                    append = f"/{session.label}"
                else:
                    append = ""

                fw_path += append

                if container.parents.acquisition is not None:
                    acquisition = self.get_acquisition(container)[0]
                    append = f"/{acquisition.label}"
                else:
                    append = ""

                fw_path += append

                if container.get("container_type", "analysis") == "analysis":
                    analysis = container.label
                    append = f"/{analysis}"
                else:
                    analysis = ""

                fw_path += append

            fw_paths.append(fw_path)

        return fw_paths


def test_finder():
    import os
    import flywheel

    fw = flywheel.Client(os.environ["FWGA_API"])

    # FlywheelObjectLinker:
    #
    # def __init__(
    #         self,
    #         fw,
    #         group=None,
    #         project=None,
    #         subject=None,
    #         session=None,
    #         acquisition=None,
    #         analysis=None,
    #         file=None,
    #         level=None,

    ol = FlywheelObjectLinker(
        fw,
        group="ophthalmology",
        project="Ophthalmology",
        subject="patient_20",
        level="project",
    )

    results = ol.find()

    for r in results:
        print_container(r)


if __name__ == "__main__":

    test_finder()
    # test_analysis_OnLowestLevel()

    # test_file_NotOnLowest_LevelProvided()
