from abc import ABC, abstractmethod
import logging
import re
from flywheel import ApiException, FileEntry

import types

logging.basicConfig(level="DEBUG")
log = logging.getLogger()


# project_finder = ContainerFinder(p, fw)

INVALID = "INVALID"


class ContainerFinder:
    """
    parent - flywheel container
    client - flywheel client
    truth_table - a list of container types, and if they have a direct finder of another container type:
    for example, `project` has `project.subjects.find()`, but not `project.acquisitions.find()`

    """

    def __init__(self, parent, client):
        # log.debug(f'parent type {parent}')
        self.parent = parent
        self.parent_type = getattr(parent, "container_type", "file")
        self.client = client
        self.truth_table = {
            "group": {
                "group": INVALID,
                "project": True,
                "subject": False,
                "session": False,
                "acquisition": False,
            },
            "project": {
                "group": INVALID,
                "project": INVALID,
                "subject": True,
                "session": True,
                "acquisition": False,
            },
            "subject": {
                "group": INVALID,
                "project": INVALID,
                "subject": INVALID,
                "session": True,
                "acquisition": False,
            },
            "session": {
                "group": INVALID,
                "project": INVALID,
                "subject": INVALID,
                "session": INVALID,
                "acquisition": True,
            },
        }

    @staticmethod
    def is_fwid(id):
        """Determines if a given query might be a flywheel ID or not.

        If the given query is an ID, we will later try to get the container directly by using 'fw.get_<CONTAINER>()`

        Args:
            id (str): the query to compare against the flywheel ID format

        Returns:
            (bool) - true if it is and ID format, false if not.
        """

        if not id:
            return False

        CONTAINER_ID_FORMAT = "^[0-9a-fA-F]{24}$"
        if re.match(CONTAINER_ID_FORMAT, id):
            log.debug("matches ID")
            return True
        log.debug("does not match ID")
        return False

    def has_parent_finder(self, type):
        """Checks to see if the parent `self.parent` has a built in finder for the container type `type`.

        For example, if self.parent is a `project`, and we're attempting to query type `subject`, then this returns
        True.  However, if we're attempting to query type `acquisition`, then this returns false.

        If True, this will later instruct the class to use that built in finder (ex: `project.subjects.find(<LABEL>)`).
        If False, this will later instruct the class to use the client finder
        (ex: `fw.acquisitions.find('parents.project={parent.project.id},label={<LABEL>})` )

        I tested this and using the client like that is in fact much faster than doing something like:

        results = []
        for session in project.sessions():
            results.extend(session.acquisitions.find(<LABLE>))


        Args:
            type (str): the type of flywheel container to check and see if the parent (`self.parent`) has a built-in
            finder for

        Returns:
            search_level (bool) - true if it has a built-in finder, false if not.

        """

        log.info(self.parent_type)
        if not self.parent_type:
            log.debug(
                f"no value 'container_type' on parent object {self.parent.label}. Assuming a file"
            )
            if not isinstance(self.parent, FileEntry):
                log.error(
                    f"container has no 'container_type' attribute and is not a file"
                )
                raise Exception("Invalid input container")
            log.debug(f"does not have finder type {type}")
            return False

        parent_level = self.truth_table.get(self.parent_type)
        if not parent_level:
            log.error(f"unrecognized parent container type {self.parent_type}")
            raise Exception("unrecognized parent container type")

        search_level = parent_level.get(type)
        if search_level == INVALID:
            log.error(
                f"Cannot search for {type} on {self.parent_type}.  Try reversing parent/child"
            )
            raise Exception(f"Cannot search for {type} on {self.parent_type}")

        elif search_level is None:
            log.error(f"unrecognized search container type {search_level}")
            raise Exception("unrecognized search container type")
        log.debug(f"has finder type: {search_level}")
        return search_level

    def list_finder(self, type, query):
        """
        To be populated with finding things that are lists (analysis, files, etc).
        Will probably use list comprehension to search for names or whatever.

        Returns:

        """

        object_list = getattr(self.parent, type, None)
        if not object_list:
            return []

        if type == "files":
            result = [self.parent.get_file(query)]
        else:
            analyses = self.parent.analyses
            result = [a for a in analyses if a.label == query]

        return result

    def get_client_finder(self, type, direct_from_id=False):
        """Gets a finder from the flywheel client.

        if the self.parent doesn't have a built in finder for the container type, use:
        fw.<CONTAINER_TYPE>, or if we are getting the container direct_from_id, then we want:
        fw.get_<CONTAINER_TYPE>
        ex:

        fw.projects
        fw.acquistions
        etc

        or if direect_from_id:
        fw.get_project
        fw.get_acquisition


        Args:
            type (str): the container type to get the finder for
            direct_from_id (bool): True if the query is a flywheel ID

        Returns:
            finder

        """

        try:
            if direct_from_id:
                log.debug(f'getting finder "get_{type}s"')
                finder_at_level = getattr(self.client, f"get_{type}")
            else:
                log.debug(f'getting finder "{type}s"')
                finder_at_level = getattr(self.client, f"{type}s")
        except AttributeError as e:
            log.warning(f"no finder '{type}s()' on client")
            log.exception(e)
            return None

        return finder_at_level

    def get_parent_finder(self, type, parent=None):
        """gets a finder from the self.parent container

        if the self.parent container has a built in finder for container type `type`, then this finds that
        and returns it for use.

        Args:
            type (str): the container type to get the finder for

        Returns:
            finder

        """
        if not parent:
            parent = self.parent

        try:
            finder_at_level = getattr(parent, f"{type}s")
        except AttributeError as e:
            log.warning(f"no finder '{type}s()' on parent container")
            log.exception(e)
            return None

        return finder_at_level

    def make_client_query(self, query, parent=None):
        """Generates a query for a client level finder.

        This uses fw.<container_type>.find(label=<query>,parents.<parent_type>=self.parent.id)

        Args:
            query: the label of the container to find

        Returns:
            query (str): the full query string to be used in a finder.

        """
        if not parent:
            parent = self.parent

        full_query = []
        if query is not None:
            full_query.append(f'label="{query}"')

        if parent is not None:
            full_query.append(f"parents.{parent.container_type}={parent.id}")

        if not full_query:
            log.error("No query or parent found")
            raise Exception("No query or parent")

        full_query = ",".join(full_query)

        return full_query

    def make_parent_query(self, query):
        """Geneartes a query for the parent level finder

        if self.parent has a finder for the conatiner type, create a query for that

        Could potentially be combined with "make client query", where a boolean "client" is passed in,
        and if true, appends `parents.{self.parent.container_type}={self.parent.id}` to the `label=` query

        Args:
            query: the label of the container to find.

        Returns:

        """

        full_query = ""
        if query is not None:
            full_query += f'label="{query}"'

        return full_query

    def container_finder(self, type, query):
        """gets the appropriate finder for self.parent, given a container type to find and a query.

        If self.parent has a direct finder for `type`, we will use that finder.  Otherwise, we generate a client finder
        and use that.  We also check to see if the query resenbles a flywheel id, and if it does, we attempt to directly
        use a `fw.get_<container>()` command.

        Args:
            type (str): the type of container we're looking for
            query (str): the query to use - either a lable of a flywheel ID

        Returns:
            results (generator): a python generator of the results.

        """

        is_id = self.is_fwid(query)
        has_finder = self.has_parent_finder(type)

        if is_id or has_finder is False:
            results = self.run_client_query(type, query, is_id)
        elif has_finder is True:
            results = self.run_parent_query(type, query)
        else:
            log.error(
                "actually an exception should be raised here so this should never be read.  Our little secret"
            )

        return results

    def run_parent_query(self, type, query):

        finder = self.get_parent_finder(type)
        query = self.make_parent_query(query)

        results = finder.iter_find(query)
        return results

    def run_client_query(self, type, query, is_id=False):
        log.debug("running client query")

        if is_id:
            log.debug("is an ID, getting finder")
            finder = self.get_client_finder(type, True)
            query = query
            log.debug(f"got finder {finder}")
            log.debug("trying query")
            try:
                results = finder(query)
                if not isinstance(results, list):
                    results = [results]
                return results
            except Exception as e:
                log.warning(
                    f"query {query} resembles a flywheel id but no id matched.  Searching for label"
                )

        elif type == "group":
            finder = self.get_client_finder(type, direct_from_id=True)
            results = finder(query)
        else:
            finder = self.get_client_finder(type)
            query = self.make_client_query(query)
            log.debug(f"searching for {query}")
            log.debug(f"with finder {finder}")
            results = finder.iter_find(query)

        return results

    def find(self, type, query):
        """Finds container `type` with label or ID `query` on `self.parent`

        Args:
            type (str): the type of container we're looking for
            query (str): the query to use - either a lable of a flywheel ID

        Returns:

        """
        type = type.lower()
        if type in ["analyses", "analysis", "file", "files"]:
            standard_type = "analyses" if type in ["analyses", "analysis"] else "files"
            results = self.list_finder(standard_type, query)

        else:
            results = self.container_finder(type, query)

        return results


#
#
# project = fw.get_project('60b66777c7ecf7a9c316e3c1')
# acq_label = '1 - 00030782_000'
#
#
#
# import flywheel
# import os
# import logging
#
# logging.basicConfig(level='DEBUG')
# log=logging.getLogger()
# fw=flywheel.Client(os.environ["FWGA_API"])
# p=fw.get_project('609ed5cd75c0590334bb5313')
# project_finder = ContainerFinder(p, fw)
# project_finder.find('acquisition','nodif')
#
#
#
# a=None
# if a:
#     print('true')
# elif a is False:
#     print('false')
